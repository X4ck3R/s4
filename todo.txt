S = short term
L = long term 
------------------------------------------------
- S  - save IOCs TTPs to database, start with csv
- L  - webui
- S  - add threat intelligence
- S  - implement exa_aa
- L  - use mongodb
- LL - agent
- SS - convert to OOP 

---------------------------
WF:
	- T1
		- Ingestor
			- do connect to the host, get the files from r-dir or do run a syslog server
			- and do check messages with filters
			- and return the alert link.
		- SIEM
			- do handle the link
			- and return a raw log event.
		- Analyzer (Engine)
			- do parse the raw event
			- and do analysis
			- and do Threat intelligence
			- and return ticket objecto to 
	- T2