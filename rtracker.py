#!/usr/bin/env python2.7

## Author: Sleiman Ahmad
## Email: Sleiman.xsp@gmail.com

##v2.044

import cookielib, urllib, urllib2, ssl, json, os, re, traceback, datetime, inspect, helper
from dateutil.parser import parse

__version__ = '2.410'
colo = helper.bcolors()
class RequestTracker:
	RT_BASE_URI = 'https://check-mk.platform.nttzen.cloud:8443/REST/1.0/'
	RT_CA_CRT = 'cacerts.cer'
	RES_OK = '200 Ok'
	VERBOSE = True
	url_opener = ctx = None

	def __init__(self, user, password, cacert=None):
		if cacert is not None:
			RequestTracker.RT_CA_CRT = cacert
		RequestTracker.ctx = ssl.create_default_context(cafile = RequestTracker.RT_CA_CRT)
		self.cj = cookielib.LWPCookieJar()
		RequestTracker.url_opener = urllib2.build_opener(urllib2.HTTPSHandler(context=RequestTracker.ctx), urllib2.HTTPCookieProcessor(self.cj))
		self.access_user = user
		self.access_password = password		

	def rt_login(self):
		'''
		login to RT. password should be base64 encoded.
		'''
		try:
			rt_auth_data = urllib.urlencode({'user': self.access_user, 'pass': self.access_password.decode('base64')})
			res = self.url_opener.open(RequestTracker.RT_BASE_URI, rt_auth_data).read()
			self.__print(res)
			
			return True if RequestTracker.RES_OK in res else False
		except:
			self.__print(traceback.format_exc())
			print('RT#501: Not able to login')
			return False

	def rt_open_ticket(self, data):
		'''
		Open a ticket.
		'''	
		try:
			uri = RequestTracker.RT_BASE_URI + 'ticket/new'
			data = {'content':data}
			data = urllib.urlencode(data)
			
			res = self.url_opener.open(uri, data).read()	
			
			if RequestTracker.RES_OK in res:
				tid = re.findall('Ticket (\d+) created', res)
				if len(tid) > 0:
					return tid[0]
			return False
		except Exception as e:
			self.__print(traceback.format_exc())
			print('RT#502-0: Error open ticket %s\n%s' %(str(e), traceback.format_exc()))
			return False
	
	def rt_get_ticketPost_PAN_MALi_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):
	
		def __get_acc(s_user, d_user):
			acc = ''
			if s_user is None and d_user is None:
				return acc
			if s_user is None:
				return d_user
			if d_user is None:
				return s_user
			return '%s, %s' %(s_user, d_user)
		#data = json.loads(data)
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject #'EMEA Malicious Activity Internal PAN All'	
			accounts = data['User'] if not 'D-User' in data else __get_acc(data['User'], data['D-User'])
			apps = data['Service']
			src_ip = data['Source-IP']
			dst_ip = data['Destination-IP']
			sig = data['Threat-Name']
			sig_id = sig[sig.find("(")+1:sig.find(")")]
			occ_time = alert_time#data['Event-Time']
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			self.__print(traceback.format_exc())
			print('RT#502-1: Error open ticket %s\n%s' %(str(e), traceback.format_exc()))
			return False		

	def rt_get_ticketPost_FG_MALi_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):
		#data = json.loads(data)
		## PARSER = 'FW-FG-UTM'
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject ##'BS Malicious Activity Internal Fortigate UTM All'	
			accounts = data['User']
			apps = data['Service']
			src_ip = data['Source-IP']
			dst_ip = data['Destination-IP']
			sig = data['Attack/Virus']
			sig_id = ''#sig[sig.find("(")+1:sig.find(")")]
			occ_time = alert_time#data['Event-Time']
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False

	def rt_get_ticketPost_TM_EpBehavior_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):
		#data = json.loads(data)
		## PARSER = 'FW-TM-BM'
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject ##'BS Endpoint Behavior All'	
			accounts = data['Source-Host']
			apps = data['Process']
			src_ip = data['Source-IP']
			dst_ip = ''
			sig = 'Endpoint Behavior'
			sig_id = data['Policy']#sig[sig.find("(")+1:sig.find(")")]
			occ_time = alert_time#data['Event-Time']
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False

	def rt_get_ticketPost_TM_EpCnCSA_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):
		#data = json.loads(data)
		## PARSER = 'FW-TM-CNC' 'FW-TM-NCIE'
		try:
			queue = tkt_q 
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject 
			accounts = '' if not 'Source-Host' in data else data['Source-Host']
			apps = data['Process']
			src_ip = data['Source-IP']
			dst_ip = data['Destination-IP']
			sig = 'CnC and Suspicious Connection'
			sig_id = '' if not 'Threat' in data else data['Threat']
			occ_time = alert_time
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False

	def rt_get_ticketPost_TM_EpARequired_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):
		#data = json.loads(data)
		## PARSER = 'FW-TM-AV'
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject ##'BS Endpoint Behavior All'	
			accounts = data['Destination-Host']
			apps = data['File-Name']
			src_ip = data['Destination-IP']
			dst_ip = ''
			sig = data['Threat']#'Endpoint Behavior'
			sig_id = ''#sig[sig.find("(")+1:sig.find(")")]
			occ_time = alert_time#data['Event-Time']
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False

	def rt_get_ticketPost_TM_EpSARequired_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):
		#data = json.loads(data)
		## PARSER = 'FW-TM-SD'
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject ##'BS Endpoint Spyware Action Required All'	
			accounts = '%s, %s' %(data['User'], data['Destination-Host'])
			apps = data['File-Name']
			src_ip = data['Destination-IP']
			dst_ip = ''
			sig = data['Virus-Name']#'Endpoint Behavior'
			sig_id = data['File-Hash']#sig[sig.find("(")+1:sig.find(")")]
			occ_time = alert_time#data['Event-Time']
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False

	def rt_get_ticketPost_WIN_AD_TBFA_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time, src_ip_add):
		#data = json.loads(data)
		## PARSER = 'FW-TM-SD'
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject 
			accounts = data['Source-Host'] if data['User'] == '' else '%s, %s' %(data['User'], data['Source-Host'])
			apps = ''
			src_ip = src_ip_add
			dst_ip = ''
			sig = 'Targeted Bruteforce Attack'
			sig_id = ''
			occ_time = alert_time#data['Event-Time']
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False
	def rt_get_ticketPost_Cortex_EEMARA_ALL(self, tkt_q, assignee, tkt_subject, requestors, cc, tkt_body, data, alert_time):

		def __get_acc(user, host):
			acc = ''
			if user is None and host is None:
				return acc
			if user == 'N/A' and host == 'N/A':
				return acc
			if (user is None or user == 'N/A'):
				return host
			if (host is None or host == 'N/A'):
				return user
			return '%s, %s' %(user, host)
			
		## PARSER = 'FW-PAN-CORTEX-THREAT'
		try:
			queue = tkt_q #'Trash-Test'
			if assignee == None:
				assignee = self.__rt_get_available_assignee()
			body = tkt_body
			subj = tkt_subject ##'EMEA Endpoint Malware Action Required All'	
			accounts = __get_acc(data['User'], data['Host'])
			apps = '' if (data['File-Name'] is None or not data['File-Name']) else data['File-Name'] 
			src_ip = '' if not data['Source-IP'] else data['Source-IP']
			dst_ip = ''
			sig = '' if not data['Hash'] else data['Hash']
			sig_id = '' if not data['CY-Status-ID'] else data['CY-Status-ID']
			occ_time = alert_time
			tkt = self.__rt_build_ticket(queue, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
			return tkt
		except Exception as e:
			print str(e)
			return False				
	def __rt_get_available_assignee(self, slist_Tier2=None):
		print ('__rt_get_available_assignee')
		##working hours:
		list_Tier2 = ['ahmads', 'ahmads', 'ahmads', 'ahmads']
		return list_Tier2[0]

	def __rt_build_ticket(self, Q, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time):
		try:
			x = '''
id: ticket/new
Queue: %s
Owner: %s
Subject: %s
Status: new
Priority: 0
InitialPriority: 0
FinalPriority: 0
Requestors: %s
Cc: %s
AdminCc:
Content-Type: text/html; charset="UTF-8"
TimeEstimated: 0
Text: %s
CF.{Client Incident ID}:
CF.{Case Incident ID}:
CF.{Incident Involved Accounts}: %s
CF.{Incident Involved Applications}: %s
CF.{Incident Source IP addresses}: %s
CF.{Incident Target IP addresses}: %s
CF.{Signature Incident}: %s
CF.{Signature Incident ID}: %s
CF.{Tier1_working_time_elapsed}:
CF.{Tier2_working_time_elapsed  }:
CF.{Incident Occurence Time}: %s
CF.{Role}: 4
'''
			return x %(Q, assignee, subj, requestors, cc, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
		except Exception as e:
			self.__print(traceback.format_exc())
			print('RT#501-0: __rt_build_ticket %s\n%s' %(str(e), traceback.format_exc()))
			return False
	
	def rt_comment_ticket(self, tid, tbody):
		'''
		comment ticket
		'''
		try:
			uri = RequestTracker.RT_BASE_URI + 'ticket/%s/comment' %tid
			data = 'id: %s\nAction: comment\nContent-Type: text/html; charset="UTF-8"\nText: %s' %(tid, tbody)
			data = {'content':data}
			data = urllib.urlencode(data)
			res = self.url_opener.open(uri, data).read()
			
			if RequestTracker.RES_OK in res:
				if re.findall('Comments added', res):
					return True
			return False
		except Exception as e:
			self.__print(traceback.format_exc())
			print('RT#502-1: Error open ticket %s' %str(e))
			return False

	def rt_get_ticket_prop(self, tid):
		'''
		Get Tikcet Props. returns multilines 
		'''
		try:
			uri = RequestTracker.RT_BASE_URI + 'ticket/%s/show' %tid
			res = self.url_opener.open(uri).read().split('\n')
			self.__print(res)
			return res[2:-2] if (RequestTracker.RES_OK in res[0] and 'No ' not in res[2]) else False
		except:
			self.__print(traceback.format_exc())
			print('RT#502-2: Error retrieve props')
			return False

	def __rt_list_ticket_attach(self, tid):
		uri = RequestTracker.RT_BASE_URI +'ticket/%s/attachments/' %tid
		res = self.url_opener.open(uri).read()
		self.__print(res)
		return res if RequestTracker.RES_OK in res else False

	def __rt_read_ticket(self, tid):
		attid = self.__rt_get_ticket_attid(tid)
		if not attid:
			return False
		uri = RequestTracker.RT_BASE_URI + 'ticket/%s/attachments/%s' %(tid, attid)
		res = self.url_opener.open(uri).read()
		self.__print(res)
		return res if RequestTracker.RES_OK in res else False

	def __rt_get_ticket_attid(self, tid):
		try:
			for x in self.__rt_list_ticket_attach(tid).split('\n'):
				if 'text/html' in x:
					return (x.split(':')[0] if (x.split(':')[0].strip()).isdigit() else x.split(':')[1]).strip()
		except Exception as e:
			self.__print(traceback.format_exc())
			print ('RT#503: error getting attid')
			return False

	def rt_get_alert_from_ticket(self, tid):
		tkt = self.__rt_read_ticket(tid)
		try:
			links = re.findall('rt: <a href="(.*)">', tkt)
			if links is not None and len(links) > 0:
				return links
			return False
		except Exception as e:
			self.__print(traceback.format_exc())
			print ('RT#504: Error getting alerts')
			return False

	def rt_find_new_tickets(self, _usr):
		qry = "query=Owner='%s'AND(Status='new')" %(_usr)
		return self.__rt_search(qry)

	def rt_find_ticket_byID(self, _id):
		qry = "query=id='%s'" %(_id)
		return self.__rt_search(qry)

	def rt_find_IOCs(self, tid, queue, iocs):
		self.__print(iocs)
		##	  0			1		   2			3			4
		## CF_inv_acc, tCF_inv_app, tCF_inv_sip, tCF_inv_dip, tCF_inc_sig
		q = "query=Queue='%s'" %queue
		def __get_clause(idx, val):
			self.__print(val)
			clauses = [" AND 'CF.{Incident Involved Accounts}' = '%s'" %val,
		" AND 'CF.{Incident Involved Applications}' = '%s'" %val,
		" AND 'CF.{ 	Incident Source IP addresses}' = '%s'" %val,
		" AND 'CF.{Incident Target IP addresses}' = '%s'" %val,
		" AND 'CF.{Signature Incident}' = '%s'" %val,
		" AND 'CF.{Signature Incident ID}' = '%s'" %val
		]
			#exception_clause " OR 'CF.{Incident Involved Accounts}' = '%s'" %val,
			return clauses[idx]
		## check if two IOCs in one filed separated by ,
		found = []
		for idx, ioc in enumerate(iocs):
			if ioc is not None:
				ioc = ioc.split(',')[0] if ',' in ioc and idx == 0 else ioc
				qry = q + __get_clause(idx, ioc)
				self.__print ('handling query: %s\n' %qry)
				res =  self.__rt_search(qry)
				if not res or (len (res) == 1 and res[0].split(':')[0] == tid): continue
				found.append([ioc, [_id.split(':')[0] for _id in res]])
		return found if found else False

	def __rt_search(self, qry):
		uri = RequestTracker.RT_BASE_URI + 'search/ticket?%s' %urllib.quote(qry, safe ='=/\\')
		self.__print ('uri: %s\n' %uri)
		request = urllib2.Request(uri)
		request.add_header('Accept','*/*')
		request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0')
		request.add_header('Referer', 'https://check-mk.platform.nttzen.cloud:8443/REST/1.0/')
		res = self.url_opener.open(request).read().split('\n')
		self.__print (res)
		return res[2:-3] if ('200 Ok' in res[0] and 'No ' not in res[2]) else False

	def __print(self, msg):
		if RequestTracker.VERBOSE:
			_caller = inspect.stack()[1][3]
			str = "%s - [%s] - %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), colo.format(_caller, 'WARNING'), msg)
			print (str)
			
	def getVersion(self):
		return __version__