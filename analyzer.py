#-------------------------------------------------------------------------------
# Name:		module1
# Purpose:
#
# Author:	  AhmadSl
#
# Created:	 04/04/2019
# Copyright:   (c) AhmadSl 2019
# Licence:	 <your licence>
#-------------------------------------------------------------------------------
## v 1.412

import json
import re, os, inspect, datetime
import ipaddress, helper, ast

VERBOSE = False
colo = helper.bcolors()
__version__ = '3.141'

def getVersion():
	return __version__
	
def getPANThreat(raw_msg):
	##FUTURE_USE, 4 Receive Time, 5 Serial Number, 6 Type, 7 Subtype, FUTURE_USE, Generated Time, 10 Source IP, 11  Destination IP, 12 NAT Source IP, 13 NAT Destination IP, 14 Rule Name, 15 Source User, 16 Destination User, 17 Application, Virtual System, Source Zone, Destination Zone, Ingress Interface, Egress Interface, Log Forwarding Profile, FUTURE_USE, Session ID, Repeat Count, 27 Source Port, Destination Port, NAT Source Port, NAT Destination Port, Flags, 32 Protocol, 33 Action, Miscellaneous, 35 Threat ID, Category, 37 Severity, 38 Direction, Sequence Number, Action Flags, Source Location, Destination Location, FUTURE_USE, Content Type, PCAP_id, Filedigest, Cloud, URL Index, User Agent, File Type, X-Forwarded-For, Referer, Sender, Subject, Recipient, Report ID, Device Group Hierarchy Level 1, Device Group Hierarchy Level 2, Device Group Hierarchy Level 3, Device Group Hierarchy Level 4, Virtual System Name, Device Name, FUTURE_USE,
	##"date, cat, sub, s-ip, d-ip, snat, dnat, user, app, virus, sig, sig-id, action, s-port, d-port, severity, direction, d-user\n"
	s = raw_msg.split(',')
	return ("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" %(s[4], s[6], s[7], s[10], s[11], s[12], s[13], s[15], s[17], s[34].replace('"',''), s[35], s[35][s[35].find("(")+1:s[35].find(")")], s[33], s[27], s[28], s[37], s[38], s[16]))

	
def _mail_params_PAN(parsed_msg):
	_params = None
	_params = {
	'Event-Time': parsed_msg[0],
	'Action':parsed_msg[12],
	'Source-IP':parsed_msg[3],
	'Destination-IP':parsed_msg[4],
	'S-Port': parsed_msg[13],
	'D-Port': parsed_msg[14],
	'User':parsed_msg[7] if parsed_msg[7] != '' else None,
	'D-User':parsed_msg[17] if parsed_msg[17] != '' else None,
	'File-Name':parsed_msg[9],
	'Threat-Name':parsed_msg[10],
	'Service':parsed_msg[8],
	'Type/Subtype':'%s/%s'%(parsed_msg[1],parsed_msg[2]),
	'Severity':parsed_msg[15],
	'Direction':parsed_msg[16],
	'_parser':'FW-PAN-THREAT'
	}
	return _params
def getPANThreatAction(x):
	return {
		'alert':'the threat or URL was detected but not blocked',
		'allow':'flood detected and raised an alert',
		'deny':'the file was blocked',
		'drop':'the threat was detected and associated session was dropped',
		'reset-client':'the threat was detected and a TCP RST was sent to the client',
		'reset-server':'the threat was detected and a TCP RST was sent to the server',
		'reset-both':'the threat was detected and a TCP RST was sent to both the client and the server',
		'block-url':'the URL request was blocked because it matched a URL category that was set to be blocked',
		'block-ip':'the threat was detected and client IP was blocked',
		'random-drop':'a flood was detected and packet was randomly dropped',
		'sinkhole':'a DNS sinkhole was activated',
		'syncookie-sent':'a syncookie alert was generated',
		'block-continue':'a HTTP request was blocked and redirected to a Continue page with a button for confirmation to proceed',
		'continue':'a response to a block-continue URL continue page indicating a block-continue request was allowed to proceed',
		'block-override':'a HTTP request was blocked and redirected to an Admin override page that requires a pass code from the firewall administrator to continue',
		'override-lockout':'too many failed admin override pass code attempts from the source IP and is now blocked from the block-override redirect page',
		'override':'response to a block-override page where a correct pass code is provided and the request is allowed'
	}.get(x, '')

def analyzePANThreat(raw_msg):
	p = getPANThreat(raw_msg).split(', ')
	return p[12]

def composeEmail(raw_msg):
	parsed = getPANThreat(raw_msg).split(', ')
	return _mail_params_PAN(parsed)
	
	##legacy:
	##  0   1   2   3   4   5   6   7   8   9	   10  11	  12  13  14 15 16
	##"date, cat, sub, s-ip, d-ip, snat, dnat, user, app, virus, sig, sig-id, action, s-port, d-port, severity, direction\n"	
	s_body = ''
	p = getPANThreat(raw_msg).split(', ')
	s_body += ("On date: %s, SIEM has registered that the user " %(p[0]))
	s_sip  = ("associated with the ip [%s], has used " %p[3])
	s_user = ("[%s], " %p[7])
	s_app  = ("the application [%s] " %(p[8]))
	s_body += (s_user + s_sip,  s_sip)[p[7]=='']
	s_body += (s_app, "a web browser ")[p[8] == "web-browsing"]
	s_body += ("to connect to the ip [%s] and download the file [%s] which is infected by the malware [%s]. As a result, %s.\n" %(p[4],p[9],p[10],getPANThreatAction(p[12])))

	return s_body

def pan_file(src, dst):
	i = 1
	inF  = open(src, "r")
	outF = open(dst, "w")
	hdr  = "date, cat, sub, s-ip, d-ip, snat, dnat, user, app, virus, sig, sig-id, action\n"
	outF.write(hdr)
	for x in inF:
		outF.write(getPANThreat(x))
		i = i+1
	outF.close()
	##PAN: only escalate if the virus is in the two end points.
	print ("done")

#########################################
############## <Win AD ##################
#########################################
def composeWinSecAD(s):
	try:
			line = json.loads(s)
			parsed = {'_parser':'WIN-AD-SEC-BF',
			'Windows Event ID':__WinAD_get_val("event_id", line),
			'User':__WinAD_get_val("TargetUserName" ,line["event_data"]),
			'Source-IP':__WinAD_get_val("IpAddress" ,line["event_data"]),
			'Source-Host':__WinAD_get_val("WorkstationName" ,line["event_data"]),
			'Windows Substatus':__WinAD_get_val("SubStatus" ,line["event_data"]),
			'Windows Logon Type':__WinAD_get_val("LogonType" ,line["event_data"])
			}
			return parsed
	except Exception as e:
			__print(str(e))
            
def __WinAD_get_val(key, dic):
	if key in dic:
		return dic[key]
	return ''

#########################################
############# Win AD /> #################
#########################################

##########################################
##################<FGT ###################
##########################################
	
def __getFGTWebFilter(raw_evt):
	#get the original message
	s = raw_evt.split('","')[3].split('>')[1]
	s = s[:-3]
	if 'msg=:' in s or 'msg=' not in s:
		return s
	msg_sp = re.search('msg=(.*) action=', s).group(1) if re.search('msg=(.*) action=', s) else (re.search('msg=(.*) crscore=', s).group(1) if 'crscore=' in s else _exval(s, 'msg=', 'attackcontextid='))
	msg_qt = msg_sp.replace(" ", "-")
	return (s.replace(msg_sp, msg_qt))

def is_FGT_threat(ticket):
	msg = __getFGTWebFilter(ticket)
	_act = _exval(msg, 'action=')
	_sip = _exval(msg, 'srcip=')
	_dip = _exval(msg, 'dstip=')
	_dir = _exval(msg, 'direction=')
	_sub = _exval(msg, 'subtype=')
	_usr = _sip if (_exval(msg, 'user=')) is not None else _exval(msg, 'user=')
	res = False if _act == "blocked" else (ipaddress.ip_address(_sip).is_private and ipaddress.ip_address(_dip).is_private)
	return 'An %s %s to %s was %s, there is %sThreat!\nSleiman' %(_dir, _sub, _usr, _act, '' if res else 'no ')

def parseFG(evt):
	msg = __getFGTWebFilter(evt)
	_time = '%s %s' %(_exval(msg, 'date='), _exval(msg, 'time='))
	_act = _exval(msg, 'action=')
	_sip = _exval(msg, 'srcip=')
	_dip = _exval(msg, 'dstip=')
	_dir = _exval(msg, 'direction=')
	_sub = '%s/%s' %(_exval(msg, 'type='), _exval(msg, 'subtype='))
	_servcice = _exval(msg, 'service=')
	_attack = _exval(msg, 'attack=') if 'attack=' in msg else _exval(msg, 'virus=')
	_level = _exval(msg, 'crlevel=')
	if 'msg=' in msg:
		_msg = _exval(msg, 'msg=', 'crscore=') if 'crscore=' in msg else _exval(msg, 'msg=', 'attackcontextid=')
		##avoid assymetric log line exceptions
		if not _msg.startswith('misc:'):
			_msg = _msg.split('=')[0].rsplit(' ', 1)[0]
	else:
		_msg = None
	_usr = _exval(msg, 'user=')
	_severity = _exval(msg, 'severity=')
	_host = _exval(msg,'hostname=')
	res = False if _act == "blocked" else (ipaddress.ip_address(_sip).is_private and ipaddress.ip_address(_dip).is_private)
	return(_time, _act, _sip, _dip, _sub, _usr, _servcice, _attack, _level, _msg, res, _severity, _host)
	#		0		1	2		3	4		5		6		7		8		9		10		11		12

def _mail_params_FG(parsed_msg):
	_params = {
	'Event-Time':parsed_msg[0],
	'Action':parsed_msg[1],
	'Source-IP':parsed_msg[2],
	'Destination-IP':parsed_msg[3],
	'User':parsed_msg[5],
	'Type/Subtype':parsed_msg[4],
	'Service':parsed_msg[6],
	'Attack/Virus':parsed_msg[7],
	'Criticality':parsed_msg[8],
	'Message':parsed_msg[9],
	'Is-Threat':'Yes' if parsed_msg[10] else 'No',
	'Severity':parsed_msg[11],
	'Host':parsed_msg[12],
	'_parser':'FW-FG-UTM'
	}
	return _params

def composeFG(evt):
	pm = parseFG(evt)
	return _mail_params_FG(pm)

def FGT_file(src, dst):
	i = 1
	inF  = open(src, "r")
	#outF = open(dst, "w")
	#hdr  = "date, cat, sub, s-ip, d-ip, user, direction, action\n"
	#outF.write(hdr)
	for x in inF:
		#outF.write(getPANThreat(x))
		print (str(i) + ": ")
		is_FGT_threat(x)
		i = i+1

	#outF.close()
	##PAN: only escalate if the virus is in the two end points.
	print ("done")
##########################################
################## FGT>###################
##########################################

##########################################
##################<TM ###################
##########################################

def getTM_BM_CS2_Policy(x):
	return {
		'0':'Compromised executable file',
		'1':'New startup program',
		'2':'Host file modification',
		'3':'Program library injection',
		'4':'New Internet Explorer plugin',
		'5':'Internet Explorer setting modification',
		'6':'Shell modification',
		'7':'New service',
		'8':'Security policy modification',
		'9':'Firewall policy modification',
		'10':'System file modification',
		'11':'Duplicated system file',
		'13':'Layered service provider',
		'14':'System process modification',
		'16':'Suspicious behavior',
		'100':'Newly encountered programs',
		'200':'Unauthorized fileencryption',
		'1000':'Threat behavior analysis',
		'9999':'User-defined policy'
	}.get(x, '')	

def getTM_BM_CN2_Event(x):
	return {
		'1':'Process',
		'2':'Process image',
		'4':'Registry',
		'8':'File system',
		'16':'Driver',
		'32':'SDT',
		'64':'System API',
		'128':'User Mode',
		'2048':'Exploit',
		'65535':'All'
	}.get(x, '')

def getTM_BM_Action(x):
	return {
		'0':'Allow',
		'1':'Ask',
		'2':'Deny',
		'3':'Terminate',
		'4':'Read Only',
		'5':'Read/Write Only',
		'6':'Read/Execute Only',
		'7':'Feedback',
		'8':'Clean',
		'1002':'Unknown',
		'1003':'Assess',
		'1004':'Terminated. Fileswere recovered.',
		'1005':'Terminated. Somefiles were not recovered.',
		'1006':'Terminated. Fileswere not recovered.',
		'1007':'Terminated. Files were recovered.',
		'1008':'Terminated. Some files were not recovered.',
		'1009':'Terminated. Riles were not recovered.'
	}.get(x, '')  

def getTM_BM_CN3_op(x):
	return {
		'101':'Create Process',
		'102':'Open',
		'103':'Terminate',
		'104':'Terminate',
		'301':'Delete',
		'302':'Write',
		'303':'Access',
		'401':'Create File',
		'402':'Close',
		'403':'Execute',
		'501':'Invoke',
		'601':'Exploit',
		'9999':'Unhandled Operation' 
	}.get(x, '')
	
def getTM_AV_CN2_2ndAction(x):
	return {
		'0':'Unknown',
		'1':'N/A',
		'2':'Clean',
		'3':'Delete',
		'4':'Move',
		'5':'Rename',
		'6':'Pass/Log',
		'7':'Strip',
		'8':'Drop',
		'9':'Quarantine',
		'10':'Insert/Replace',
		'11':'Archive',
		'12':'Stamp',
		'13':'Block',
		'14':'Redirect mail for approva',
		'81':'Encrypted',
		'90':'Detect',
		'257':'Reset'
	}.get(x, '')

def getTM_AV_CN3_severity(x):
	return {
		'0':'Unknown',
		'1':'Information',
		'2':'Warning',
		'3':'Error',
		'4':'Critical'
	}.get(x, '')

def getTM_CNC_CN1_risk_level(x):
	return {
		'0':'Unknown',
		'1':'Low',
		'2':'Medium',
		'3':'High'
	}.get(x, '')

def getTM_CNC_CN2_list(x):
	return {
		'0':'Global List',
		'1':'Custom List',
		'2':'User Defind List'
	}.get(x, '')
	
def getTM_CNC_CN3_callbck_format(x):
	return {
		'0':'IP',
		'1':'IP',
		'2':'HTTP',
		'4':'SMTP'
	}.get(x, '')   

def getTM_NCIE_CN1_list(x):
	return {
		'0':'Global C&C pattern',
		'1':'Relevance rules',
		'2':'User-defined block list'
	}.get(x, '')

def __getTM(raw_evt):

	#get the original message
	s = raw_evt.split('","')[2].split('>')[1]
	s = s[:-3]
	return s

def parseTM(raw_evt):
	msg = __getTM(raw_evt)
	### Rules: BS Endpoint Behavior All
	if '|Behavior Monitoring|' in msg:
		_time = re.search('rt=(.*) dvchost', msg).group(1)#.split(" ")[0]
		_policy = getTM_BM_CS2_Policy(re.search('cs2=(.*) ', msg).group(1).split(" ")[0])
		if 'cn2=' in msg:
			_event = getTM_BM_CN2_Event(re.search('cn2=(.*) ', msg).group(1).split(" ")[0])
			_sproc = _exval(msg, 'sproc=','cn2Label=')
		else:
			_event = None
			_sproc = _exval(msg, 'sproc=','cn3Label=')
		_target = _exval(msg, 'cs1=','act=')
		_action = getTM_BM_Action(re.search('act=(.*) ', msg).group(1).split(" ")[0])
		if 'cn3=' in msg:
			_op = getTM_BM_CN3_op(re.search('cn3=(.*) ', msg).group(1).split(" ")[0])
		else:
			_op = None
			_sproc = _exval(msg, 'sproc=','cs3Label=')
		_shost = re.search('shost=(.*) ', msg).group(1).split(" ")[0]
		_sip = re.search('src=(.*) ', msg).group(1).split(" ")[0]
		return ('BM', _time, _policy, _event, _sproc, _target, _action, _op, _shost, _sip)
		#return  '%0	 %1		%2 |	  %3 |	   %4|	   % 5		%6 |   %7 |	%8|	 %9  
	elif '|AV:' in msg:
		_time = _exval(msg,'rt=', 'cnt=')
		_dhost = _exval(msg,'dhost=')
		_threat = msg.split('|')[5]
		_2nd_action = getTM_AV_CN2_2ndAction(_exval(msg,'cn2='))
		_act_res1 = _exval(msg,'cs5=','cs6Label=')
		_act_res2 = _exval(msg,'cs6=','cat=')
		_severity = getTM_AV_CN3_severity(_exval(msg,'cn3='))
		_fname = _exval(msg,'fname=', 'filePath=')
		_msg = _exval(msg,'msg=', 'dst=')
		_fpath = _exval(msg,'filePath=', 'msg=') if _msg is not None else (_exval(msg,'filePath=', 'dst=') if not 'c6a3Label=' in msg else _exval(msg,'filePath=', 'c6a3Label='))
		_dst = _exval(msg,'dst=')
		_filehash = _exval(msg,'fileHash=')
		return(('AV', _time, _dhost, _dst, _threat, _2nd_action, _act_res1, _act_res2, _severity, _fname, _fpath, _msg, _filehash ))
		#return('%s | %s |%s | %s | %s | %s | %s |%s | %s | %s | %s| %s'%('AV', _time, _threat, _dhost, _2nd_action, _act_res1, _act_res2, _severity, _fname, _fpath, _msg, _dst))
	# elif 'CnC XXllback' in msg:
	
		# _time = re.search('rt=(.*) cat=', msg).group(1)
		# _shost = re.search('shost=(.*) ', msg).group(1).split(" ")[0]
		# _sip = re.search('src=(.*) ', msg).group(1).split(" ")[0]
		# _domain = re.search('cs3=(.*) ', msg).group(1).split(" ")[0]
		# _policy = None if not (re.search('cs4=(.*) act=', msg)) else re.search('cs4=(.*) act=', msg).group(1)
		# _action = re.search('act=(.*) ', msg).group(1).split(" ")[0]
		# _request = None if not (re.search('request=(.*) ', msg)) else re.search('request=(.*) ', msg).group(1).split(" ")[0]
		# _dst = None if not (re.search('dst=(.*) ', msg)) else re.search('dst=(.*) ', msg).group(1).split(" ")[0]
		# _process = re.search('deviceProcessName=(.*) dvchost=', msg).group(1)
		# _risk_lvl = getTM_CNC_CN1_risk_level(re.search('cn1=(.*) ', msg).group(1).split(" ")[0])
		# _detection_list = getTM_CNC_CN2_list(re.search('cn2=(.*) ', msg).group(1).split(" ")[0])
		# return(('CnC', _time, _shost, _sip, _domain, _policy, _action, _request, _process, _risk_lvl, _detection_list, _dst ))
# ##

	elif 'Spyware Detected' in msg:
		_time   = _exval(msg,'rt=', 'cnt=')
		_dhost  = _exval(msg,'dhost=')
		_virus  = _exval(msg,'cs1=')
		_action = _exval(msg,'cs5=', 'cs6Label=')
		_filename = _exval(msg,'fname=', 'filePath=')
		_dst	  = _exval(msg,'dst=')		
		_duser	= _exval(msg,'duser=')
		_filehash = _exval(msg,'fileHash=')
		return ('SD', _time, _dhost, _dst, _duser, _virus, _action, _filename, _filehash)
	  		
	elif 'CnC Callback' in msg:
		_time   = _exval(msg,'rt=', 'cat=')
		_shost  = _exval(msg,'shost=')
		_sip	= _exval(msg,'src=')
		_dst	= _exval(msg,'dst=')
		_domain = _exval(msg,'cs3=')
		_request = _exval(msg, 'request=')		
		_policy = _exval(msg,'cs4=', 'act=')
		_action = _exval(msg,'act=')
		_process  = _exval(msg,'deviceProcessName=', 'dvchost=')
		_risk_lvl = getTM_CNC_CN1_risk_level(_exval(msg,'cn1='))
		_detection_list = getTM_CNC_CN2_list(_exval(msg,'cn2='))
		return(('CnC', _time, _shost, _sip, _domain, _policy, _action, _request, _process, _risk_lvl, _detection_list, _dst ))
	elif 'NCIE:' in msg:
		_time   = _exval(msg,'rt=', 'cat=')
		_process  = _exval(msg,'deviceProcessName=', 'act=')
		_action = _exval(msg,'act=')
		_sip	= _exval(msg,'src=')
		_dst	= _exval(msg,'dst=')
		_sport	= _exval(msg,'spt=')
		_dport 	= _exval(msg,'dpt=')
		_direction = _exval(msg, 'deviceDirection=')
		_pattern = getTM_NCIE_CN1_list(_exval(msg,'cn1='))
		_threat_name = _exval(msg,'cs2=')
		return('NCIE', _time, _process, _action, _sip, _dst, _sport, _dport, _direction, _threat_name, _pattern)
	else:
		return ['N/A']

def _exval(msg, str_start, str_end=None):
	#print ('\n*Exctract*\n(\nt%s\n\t %s, %s)\n*/Extract*\n' %(msg, str_start, str_end))
	if str_end is not None:
		return None if not (re.search('%s(.*) %s' %(str_start, str_end), msg)) else re.search('%s(.*) %s' %(str_start, str_end), msg).group(1)
	return None if not (re.search('%s(.*) ' %str_start, msg)) else re.search('%s(.*) ' %str_start, msg).group(1).split(" ")[0]
	
def _mail_params_TM(parsed_msg):
	
	_params = None
	if parsed_msg[0] == 'CnC':
		_params = {
		'Event-Time': parsed_msg[1],
		'Action': parsed_msg[6],
		'Source-Host':parsed_msg[2],
		'Source-IP': parsed_msg[3],
		'Destination-IP':parsed_msg[11],
		'Domain': parsed_msg[4],
		'Policy': parsed_msg[5],
		'Request': parsed_msg[7],
		'Process': parsed_msg[8],
		'Risk-Level':parsed_msg[9],
		'Detection-List':parsed_msg[10],
		'_parser':'FW-TM-CNC'
		}
		return _params
	elif parsed_msg[0] == 'NCIE':
	#	return('NCIE', _time, _process, _action, _sip, _dst, _sport, _dport, _direction, _threat_name, _pattern)
		#		0		1		2		3	4	5	6		7			8			9
		_params = {
		'Event-Time': parsed_msg[1],
		'Action': parsed_msg[3],
		'Source-IP': parsed_msg[4],
		'Destination-IP':parsed_msg[5],
		'S-Port': parsed_msg[6],
		'D-Port': parsed_msg[7],
		'Direcrion': parsed_msg[8],
		'Process': parsed_msg[2],
		'Threat':parsed_msg[9],
		'Pattern':parsed_msg[10],
		'_parser':'FW-TM-NCIE'
		}
		return _params	
	elif parsed_msg[0] == 'AV':
		_params = {
		'Event-Time': parsed_msg[1],
		'Action': parsed_msg[5],
		'Destination-Host': parsed_msg[2],
		'Destination-IP': parsed_msg[3],
		'Threat': parsed_msg[4],
		'Action-Results1': parsed_msg[6],
		'Action-Results2': parsed_msg[7],
		'Severity': parsed_msg[8],
		'File-Name': parsed_msg[9],
		'File-Path': parsed_msg[10],
		'File-Hash': parsed_msg[12],
		'Message': parsed_msg[11],
		'_parser':'FW-TM-AV'
		}
		return _params
	
	elif parsed_msg[0] == 'SD':
		_params = {
		'Event-Time': parsed_msg[1],
		'Action': parsed_msg[6],
		'Destination-Host': parsed_msg[2],
		'Destination-IP': parsed_msg[3],
		'User': parsed_msg[4],
		'Virus-Name': parsed_msg[5],
		'File-Name': parsed_msg[7],
		#'File-Path': parsed_msg[10],
		'File-Hash': parsed_msg[8],
		'_parser':'FW-TM-SD'
		}
		return _params
	elif parsed_msg[0] == 'BM':
		_params = {
		'Event-Time': parsed_msg[1],
		'Action': parsed_msg[6],
		'Source-Host': parsed_msg[8],
		'Source-IP': parsed_msg[9],
		'Policy': parsed_msg[2],
		'Event': parsed_msg[3],
		'Operation': parsed_msg[7],
		'Process': parsed_msg[4],
		'Target': parsed_msg[5],
		'_parser':'FW-TM-BM'
		
		}
		return _params
		
def composeTM(raw_evt):
	parsed_msg = parseTM(raw_evt)
	#print (parsed_msg)	
	if parsed_msg[0] in ['BM', 'AV', 'CnC', 'SD', 'NCIE']:
		return _mail_params_TM(parsed_msg)
	#elif parsed_msg[0] == 'AV':
		##m_body = "Hi\nOn date %s SIEM has registered that user on host [%s | %s] has tried to access the file [%s] which is infected by the malware [%s]. The firewall was %s.\nSeverity: %s\nPath: %s | %s\n" %(parsed_msg[1], parsed_msg[2], parsed_msg[3], parsed_msg[9], parsed_msg[4], parsed_msg[6], parsed_msg[8], parsed_msg[10]+parsed_msg[9], parsed_msg[11])
		#print (m_body)
		#return _mail_params_TM(parsed_msg)	
	else:
		return ('This TM incident handler is not implemented yet')
##########################################
################## TM>###################
##########################################
 
##########################################
################## <TRAPS ###################
##########################################
def getPANTrapsThreat(raw_msg):
	## get traps raw log msg
	s = raw_msg.split(',')
	return s[2:42]
#####
# deprecated
def _debug_traps(splitted):
	traps_format = '''recordType, class, FUTURE_USE, eventType, generatedTime, serverTime, agentTime, tzOffset, FUTURE_USE, facility, customerId, trapsId, serverHost, serverComponentVersion, regionId, isEndpoint, agentId, osType, isVdi, osVersion, is64, agentIp, deviceName, deviceDomain, severity, trapsSeverity, agentVersion, contentVersion, protectionStatus, preventionKey, moduleId, profile, moduleStatusId, verdict, preventionMode, terminate, terminateTarget, quarantine, block, postDetected, eventParameters(Array), sourceProcessIdx(Array), targetProcessIdx(Array), fileIdx(Array), processes(Array), files(Array), users(Array), urls(Array), description(Array)'''

	_pant_format = traps_format.split(',')
		## fix bug in length
	if len(splitted) == 44:
		__print ('bug detected')
		for i in range(5):
			splitted.append([])
	# __print (str(len(splitted) )+ '/' + str(len(_pant_format)))
	# for i in range(0, len(splitted)):
		# if len(splitted) == len(_pant_format):
			# __print (str(i) + '	 ' + _pant_format[i], + ' -> '  + splitted[i])
		# else:
			# __print (str(i) + ' -> '  + splitted[i])
	return splitted
# deprecated	
def _traps_array_fix(_raw):
	_new = _raw

	##### bug! logline is truncated
	subcats = ['WildFire Malware','Behavioral Threat','Local Analysis Malware','Suspicious File Modification']
		
	if not [j for j in subcats if j in _new]:
		return _new.split(',[')[0]
	##### bug! logline is truncated
	
	arrays = re.findall('\[.*?\]', _raw)
	if len(arrays) > 0:
		for array in arrays:
			_arr = array.replace(',', ';s4;')
			_new = _new.replace(array, _arr)

	## handle Exception when Behavioral threat
	commas = re.findall('\[\{\'rawFullPath(.*)\],\[\{\'userName', _new)
	if commas:	
		_new = _new.replace(commas[0], commas[0].replace(',',';s4;'))
	return _new
# deprecated	
def _traps_getFiles(files_array):
	
	if not files_array:
		return None, None, None
	try:
		files_array = files_array.encode('unicode-escape')
		elements = ast.literal_eval(files_array.replace(';s4;',','))
		fpath = []
		fname = []
		fhash = []
		for el in elements:
			if el['rawFullPath']:
				fpath.append(el['rawFullPath']) 
			if el['fileName']:
				fname.append(el['fileName'])
			if el['sha256']:
				fhash.append(el['sha256'])
		## get unique values only
		fpath = list(set(fpath))    
		fname = list(set(fname))
		fhash = list(set(fhash))
	except Exception as e:
		return None, None, None
	return (", ".join([str(i) for i in fpath])), (", ".join([str(i) for i in fname])), (", ".join([str(i) for i in fhash]))
# deprecated
def _traps_getUser(user_array):
	if not user_array:
		return None
	elements = ast.literal_eval(user_array.replace(';s4;',','))
	for el in elements:
		return None if not el['userName'] else el['userName']
###

def _get_CortexXDR(_new):
	user = re.findall(',\[\{\'userName\': \'(.*?)\'', _new)
	fpath = list(set(re.findall(',\[\{\'rawFullPath\': \'(.*?)\'', _new)))    
	fname = list(set(re.findall(', \'fileName\': \'(.*?)\'', _new)))
	fhash = list(set(re.findall(', \'sha256\': \'(.*?)\'', _new)))
	
	return ('' if len(user) == 0 else user[0], (", ".join([str(i) for i in fpath])), (", ".join([str(i) for i in fname])), (", ".join([str(i) for i in fhash])))
		
def mail_params_TrapsCortex(meta, s):
	def _chkval(v):
		## checks val in numerical
		return -1 if v == 'None' else int(v)
	
	# deprecated
	#fpath, fname, fhash = _traps_getFiles(s[45])
	#user = _traps_getUser(s[46])
	# 
	
	user, fpath, fname, fhash = meta
	if user == 'N/A':
		user = ''
	_actions = '%s, %s, %s' %('Terminated' if _chkval(s[35]) == 1 else 'Not Terminated', 'Quarantined' if _chkval(s[37]) == 1 else 'Not Quarantined', 'Blocked' if _chkval(s[38]) == 1 else 'Not Blocked')
	data = {'Event-Time': s[4],
	'Host':s[22],
	'Source-IP':s[21],
	'Domain':s[23],
	'Severity':'High' if _chkval(s[24]) == 3 else s[24],
	#'Blocked':'Yes' if _chkval(s[38]) == 1 else 'No',
	#'Quarantined':'Yes' if _chkval(s[37]) == 1 else 'No',
	#'Terminated':'Yes' if _chkval(s[35]) == 1 else 'No',
	'CY-Status-ID': s[32],
	'File-Name': fname,
	'File-Path': fpath,
	'Hash': fhash,
	'User':user,
	'Action': s[34],
	'Actions': _actions,
	'_parser':'FW-PAN-CORTEX-THREAT'
	}
	return data

def composeTrapsCortex(raw_msg):
	_meta = _get_CortexXDR(raw_msg)
	strin_split = getPANTrapsThreat(raw_msg)
	return mail_params_TrapsCortex(_meta, strin_split)

#deprecated	
def traps_cortex_test():
	with open('source.txt', 'r') as f:
		lines = f.readlines()
		norm = []
		for x in lines:
			string_fixed = _traps_array_fix(x)
			strin_split = getPANTrapsThreat(string_fixed)
			strin_processed = _debug_traps(strin_split)		
			norm.append(mail_params_TrapsCortex(strin_processed))

	with open ('norm', 'w') as out:
		for n in norm:
		#print norm
			out.write(str(n) + '\n')

##########################################
################## TRAPS>###################
##########################################
def handle_event(evt):
	#print ('\n*\n%s\n*\n') %evt
	__print('Raw Event:\n*\n%s\n*\n' %evt)
	if ('_FW_FG-' in evt):
		#print ('FG')
		return composeFG(evt)
	elif ('_FW_PAN-' in evt):
		#print ('PAN')
		return composeEmail(evt)
	elif ('manage.trendmicro.com' in evt):
		#print ('TM')
		return composeTM(evt)
	elif ('computer_name' in evt and '_AD_WINSEC_' in evt):
		return composeWinSecAD(evt)
	elif ('"collector":"DMZ"' in evt and ' cortexxdr ' in evt):
		return composeTrapsCortex(evt)
	
	#print 'Unknow Event:\n**\n%s\n**\n' %evt
	return {'code':'-1','Status':'Failed', 'Reason':'No handler for this event', 'Event':evt}
def __print(msg):
	if VERBOSE:
		_caller = inspect.stack()[1][3]
		str = "%s - [%s] - %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), colo.format(_caller, 'WARNING'), msg)
		print (str)