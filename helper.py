#!/usr/bin/env python2.7

### S4'IR - Helper
##{* Sleiman's Smart Sleeping SOAR *}###
### Author: Sleiman Ahmad
###


__version__ = '0.311'

import smtplib 
import base64, binascii
from email.mime.multipart import MIMEMultipart 
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
from os.path import basename


def getEmailTableRow(t_header, t_data):
		t_row = '<tr><td class="property" >&nbsp;'+t_header+'</td><td>'+t_data+'<br></td></tr>';
		return t_row


		
def send_email(_from, _to, _subj, _body, _attach=None ):
	try:
		fromaddr = _from 
		toaddr = _to
		# instance of MIMEMultipart 
		msg = MIMEMultipart() 
		msg['From'] = fromaddr 
		msg['To'] = toaddr 
		msg['Subject'] = _subj
		body = _body
		# attach the body with the msg instance 
		msg.attach(MIMEText(body, 'html')) 
		for f in _attach or []:
			with open(f, "rb") as fil:
				part = MIMEApplication(
					fil.read(),
					Name=basename(f)
				)
			# After the file is closed
			part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
			msg.attach(part)
		
		s = smtplib.SMTP('smtp1.aruba.it', 587) 
		# start TLS for security 
		s.starttls() 
		s.login(fromaddr, 'Zen2019Zen!!!') 
		s.sendmail(fromaddr, toaddr, msg.as_string()) 
		s.quit()
		return True
	except Exception as e:
		print("Error #301: %s" %(str(e)))
		return False
		
def send_report(_values, to):
	_from = "system-noreply@nttzen.cloud"
	_to = to
	_subj = "[EMEA-BS][S4] - Non handled alert"
	
	
	_htmlWrapper_hdr = '<!-- Sleiman`s Smart Sleeping Solutions - Incident Handler[S4-IH] --><!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style>.s4Table{width:750px;background-color:#fff;border-spacing:5px;font-size:14px;border:0}.s4Table td{padding:10px}.property{background-color:#c6454b;width:30%;color:#fff;height:20px;padding:5px}.tableHeader{background-color:#eb2136;text-align:center;font-size:28px;color:#fff;padding:10px}.property a:link{color:#fff}.property a:visited{color:#fff}.ex2{max-width:500px;}</style></head><body style="font-size: 14px; font-family: Calibri, Segoe UI, arial, sans-serif;">'
	
	_html_body = '<table class="s4Table" align="center">'
	_html_body += '<tr><td colspan="2" class="tableHeader">S4-IH | EMEA-BS<br></td></tr></table>'
	
	_body_message = "This is to notify you that S4 might not have handled successfully the following alert. Please double check on the RT System and inform the administrator about this warning.";
	
	_html_body += '<table align="center" border="0" style="width: 750px; background-color: white;"><tbody><tr> <td "> <table class="aes-footer-content" style="-webkit-margin-start: 0px; -webkit-margin-end: 0px; width: 100%" border="0" cellpadding="0" cellspacing="0" align="center"> <tbody> <tr> <td valign="top" style="background-color: #fff; font-family:Calibri, Arial, sans-serif;font-size:14px;padding-top: 15px;padding-right: 1px;padding-bottom: 2px;padding-left: 1px;color:#000000"> <p align="left" style="margin-bottom:1em;">'+ _body_message +'</p></td> </tr> </tbody> </table> </td> </tr> </tbody></table>';
	
	_html_body += '<table class="s4Table" align="center">'
	_html_body += getEmailTableRow('S4 Alert Time', str(_values[0]))
	_html_body += getEmailTableRow('Tenant', _values[1])
	_html_body += getEmailTableRow('SIEM Rule', _values[2])
	_html_body += getEmailTableRow('Alert Link', '<a href=%s>Click here to open</a>' %_values[3])
	_html_body += getEmailTableRow('Reason of Failure', _values[4])
	_html_body += '</table>';
	
	_htmlWrapper_ftr = '<table align="center" border="0" style="width: 750px; background-color: white;"><tbody><tr> <td style="border-top: solid 2px #898989;"> <table class="aes-footer-content" style="-webkit-margin-start: 0px; -webkit-margin-end: 0px; width: 100%" border="0" cellpadding="0" cellspacing="0" align="center"> <tbody> <tr> <td valign="top" style="background-color: #f5f5f5; font-family:Calibri, Verdana, Arial, sans-serif;font-size:12px;padding-top: 0px;padding-right: 5px;padding-bottom: 1px;padding-left: 5px;color:#000000"> <p align="left" style="margin-bottom:0.1em;"></p> <p align="left" style="margin-bottom:0;"><b>S4</b><br>Incident Handler<br>Zen Security Operations Center<br>NTT Data Italia <br></p></td></tr> </tbody> </table> </td> </tr> </tbody></table></div></body></html>';
	
	htmlDocument = _htmlWrapper_hdr + _html_body + _htmlWrapper_ftr
	_txt = htmlDocument
	return (send_email(_from, _to, _subj, _txt, None))


		
class bcolors: 
	def get_color(self, x):
		return {
	'HEADER':'\033[95m',
	'OKBLUE':'\033[94m',
	'OKGREEN':'\033[92m',
	'WARNING':'\033[93m',
	'FAIL':'\033[91m',
	'ENDC':'\033[0m',
	'BOLD':'\033[1m',
	'BLINK':'\033[5m',
	'MAGENTA':'\033[95m',
	'CYAN':'\033[96m',
	'UNDERLINE':'\033[4m'
	}.get(x, '')
	def format(self, msg, type):
		return '%s%s%s' %(self.get_color(type), str(msg), self.get_color('ENDC'))


def is_base64(s):
	try:
		base64.decodestring(s)
		return True
	except binascii.Error:
		return False

def getVersion():
	return __version__
	
#send_report(None, None)