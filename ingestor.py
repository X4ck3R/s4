#!/usr/bin/env python2.7

## Author: Sleiman Ahmad
## Email: Sleiman.xsp@gmail.com
import subprocess, os, shutil, json, re
import traceback, datetime, inspect, paramiko, helper, hashlib

R_HOST = '192.168.130.27'
R_PORT = 22
R_USER = 'root'
R_PASS = 'WmVuWmVuWmVuMjAxOCEhIQ=='
R_LOGS = '/tmp/d/*'
L_LOGS = '/tmp/d/'

VERBOSE = False
ENABLED = False

def __init(host, port, user, password, rlogs, llogs):
	return

def __print(msg):
	if VERBOSE:
		_caller = inspect.stack()[1][3]
		str = "%s - [%s] - %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), _caller, msg)
		print (str)
		
def create_sftp_client(host, port, username, password, keyfilepath, keyfiletype):
	sftp = None
	key = None
	transport = None
	try:
		if keyfilepath is not None:
			# Get private key used to authenticate user.
			if keyfiletype == 'DSA':
				# The private key is a DSA type key.
				key = paramiko.DSSKey.from_private_key_file(keyfilepath)
			else:
				# The private key is a RSA type key.
				key = paramiko.RSAKey.from_private_key(keyfilepath)
		# Create Transport object using supplied method of authentication.
		transport = paramiko.Transport((host, port))
		transport.connect(None, username, password, key)
		sftp = paramiko.SFTPClient.from_transport(transport)
		return sftp
	except Exception as e:
		__print(colo.format('An error occurred creating SFTP client: %s: %s' % (e.__class__, e), 'FAIL'))
		if sftp is not None:
			sftp.close()
		if transport is not None:
			transport.close()
		pass

def get_remote_files_names(host, port, username, password):
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	try:
		ssh.connect(host, port,username, password)
		__print ('Connected to: '+username+'@'+host+':'+str(port))
		cmd = 'find {path} -name {pattern}'
		command = cmd.format(path=R_LOGS, pattern='"*.log"')
		stdin, stdout, stderr = ssh.exec_command(command)
		res = stdout.read().splitlines()
		__print(res)
		ssh.close()
		return res
	except Exception as e:
		__print (colo.format('Error connecting to: '+username+'@'+host+':'+str(port), 'FAIL'))
		#print e
		return False

def get_file_signature(filepath):
	signature = hashlib.md5()
	with open(filepath, "rb") as f:
		for chunk in iter(lambda: f.read(4096), b""):
			signature.update(chunk)
	return signature.hexdigest()

def is_updated(file1, file2):
	try:
		if get_file_signature(file1) == get_file_signature(file2):
			return False
		return True
	except IOError as e:
		__print(colo.format('File does not exist %s: %s' % (e.__class__, e), 'FAIL'))
		return True

def get_alerts(log_path):
	__print('working dir: %s' %log_path)
	filelist = os.listdir(log_path)
	alerts = []
	rules = []
	files = []
	exceptions = []
	# res = {
	# 'alerts':[],
	# 'rules':[],
	# 'files':{'name':[], 'rules':[], 'nrules':0, 'nexeptions':0, 'nfiltered':0}
	filters = ['Bruteforce Attack SrcIP All', '-health ']
	_n_files = _n_filter = _n_alerts = _n_except = _n_lines = 0
	_stats = {}
	for i in filelist:
		try:		
			if i.endswith(".log"):
				__print('looping lines in file: %s' %i)
				with open(log_path+i) as f:
					_l_count = 0
					_is_useful_file = False
					for line in f:
						syslg_msg = json.loads(line)['message']
						_l_count +=1
						if 'exabeam' in syslg_msg:
							if any(filter in syslg_msg for filter in filters):
								#__print('escaped message at line [%d] in file: [%s]' %(_l_count, i))
								_n_filter += 1
								continue
							#__print(colo.format(syslg_msg,'WARNING'))
							_rule = re.search('ruleName="(.*?)"', syslg_msg).group(1)
							_alert_lnk = re.search('alertLink="(.*?)"', syslg_msg).group(1)
							__print(colo.format('%s - %s - %s\n' %(i, _rule, _alert_lnk), 'OKBLUE'))
							alerts.append(_alert_lnk)
							rules.append(_rule)
							_n_alerts += 1
							_is_useful_file = True
						
				_n_lines += _l_count
				if _is_useful_file:
					files.append(i)
					_n_files += 1

		except Exception as e:
			__print(colo.format(traceback.format_exc(), 'FAIL'))
			_n_except += 1
			
	_stats['files'] = _n_files
	_stats['exceptions'] = _n_except
	_stats['alerts'] = _n_alerts
	_stats['filters'] = _n_filter
	_stats['processed-lines'] = _n_lines
	return alerts, rules, files, _stats

def main(e = None, v = None):
	global ENABLED, VERBOSE 
	if e is not None:
		ENABLED = e
	if v is not None:
		VERBOSE = v
	if not ENABLED:
		print(colo.format('Module not enabled, test data enabled', 'FAIL'))
		return _x_


	if not os.path.exists(L_LOGS):
		os.makedirs(L_LOGS)
	
	files = get_remote_files_names(R_HOST, R_PORT, R_USER, R_PASS)
	sftpclient = create_sftp_client(R_HOST, R_PORT, R_USER, R_PASS, None, 'DSA')
	if not files: return False
	num_files = 0
	for file in files:
		(head, filename) = os.path.split(file)
		__print('processing %s' %file) 
		_current = '%s%s' %(L_LOGS, filename)
		_new_fpath = '%s%s%s' %(L_LOGS, filename, '.tmp')
		sftpclient.get(file, _new_fpath)
		#__print ('%s - %s' %(filename +'.tmp', get_file_signature(_new_fpath)))
		if is_updated(_new_fpath, _current):
			__print('file %s is newer than %s and will replace it' %(_new_fpath, _current))
			shutil.move(_new_fpath, _new_fpath.strip('.tmp'))
			num_files += 1
		else:
			os.remove(_new_fpath)

	sftpclient.close()
	__print('log fetching process finished. %d log files were grabbed.' %(num_files))
	return get_alerts(L_LOGS)[0]

	
colo = helper.bcolors()		
R_PASS = R_PASS.decode('base64')

_x_ = ['https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxQ_mdo8WjMoJIHbMSb', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRGaBM8WjMoJIHpAXo', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRULDvv9aFdCXAke-P', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRUM0yEeIMtP3us_K9', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRX8ny8WjMoJIHUoo_', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRh5bfv9aFdCXAEm-o', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRh6SrEeIMtP3uRZYs', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxRo4rw8WjMoJIHAP9a', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxSB9iVv9aFdCXAKomW', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxSB_z1EeIMtP3ugOjz', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxScVaL8WjMoJIH10zk', 'https://elasticsearch-host1-a:9200/exabeam-2019.08.02/exabeam-alert/AWxSfvb-v9aFdCXAGVZz']