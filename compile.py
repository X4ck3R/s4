from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
ext_modules = [
    Extension("exabeam",  ["exabeam.py"]),
    Extension("rtracker",  ["rtracker.py"]),
	Extension("analyzer",  ["analyzer.py"]),
	Extension("s4",  ["s4.py"]),
##	Extension("libs4",  ["s4.py", "exabeam.py", "helper.py", "analyzer.py", "rtracker.py"])
	Extension("helper",  ["helper.py"]),
	Extension("ingestor",  ["ingestor.py"])
#   ... all your modules that need be compiled ...

# 
# libs4rts
# libs4helper
# libs4analyzer
# libs4handler

]
setup(
    name = 'S4IR',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)