#!/usr/bin/env python2.7


### S4'IR
##{* Sleiman's Smart Sleeping SOAR *}###
### Author: Sleiman Ahmad
### v0.123
###


import cookielib, urllib, urllib2, ssl, json, os, re, traceback, datetime, analyzer, sys, helper, ingestor
import argparse
from exabeam import Exabeam
from rtracker import RequestTracker
from dateutil.parser import parse


access_user = 'ahmads'
access_password = 'MTMyTnR0QEAhIQ=='
exabeam = Exabeam(access_user, access_password)
rtracker = RequestTracker(access_user, access_password)

Exabeam.VERBOSE = RequestTracker.VERBOSE = False

args = colo = None

tier_one = False
VERBOSE = True
DIAGNOSE = False

reload(sys)
sys.setdefaultencoding('utf8')

class bcolors: 
	def get_color(self, x):
		return {
	'HEADER':'\033[95m',
	'OKBLUE':'\033[94m',
	'OKGREEN':'\033[92m',
	'WARNING':'\033[93m',
	'FAIL':'\033[91m',
	'ENDC':'\033[0m',
	'BOLD':'\033[1m',
	'UNDERLINE':'\033[4m'
	}.get(x, '')
	def format(self, msg, type):
		return '%s%s%s' %(self.get_color(type), str(msg), self.get_color('ENDC'))
	
def main():
	print ('============================\nWorking as a %s operator\n============================\n' %('Tier 1' if tier_one else 'Tier 2'))
	if not exabeam.exa_dl_login() or not exabeam.exa_aa_login():
		print ('Exabeam: Can not login')
		return
		
	if not rtracker.rt_login():
		print ('RT: Can not login')
		return
		
	if args.user:
		uba = exabeam.exa_aa_request_user(args.user)
		if not uba:
			print ('No result found for user %s' %args.user)
			return
		print ('UBA for user %s:\n' %args.user)
		print uba
		return
	
	if DIAGNOSE:

		## by ticket
		## checks certain ticket
		if DIAGNOSE.isdigit():
			print ('>> Diagnose mode enabled for ticket: %s' %DIAGNOSE)
			tkt = rtracker.rt_find_ticket_byID(DIAGNOSE)
			if tkt:
				handle_incident_t2(tkt)
		else:
		## by link
			print ('>> Diagnose mode enabled for link: %s' %DIAGNOSE)
			if 'exabeam-alert' in DIAGNOSE:
				_alert = [DIAGNOSE]
				_exa_handle_alert (_alert)
			else:
				print ('Exabeam alert link is expected')
				
			
	elif not (tier_one):
		## get alerts from the t1/rt 
		new_tikcets = rtracker.rt_find_new_tickets(access_user)
		if new_tikcets:
			print 'Hi %s! you have %s new tikcets, let me help you handling them:\n' %(access_user, colo.format(len(new_tikcets), 'WARNING'))
			handle_incident_t2(new_tikcets)
		else:
			print ("Good job %s!, there aren't any new tickets for you." %access_user)
	else:
		#print 'Enable ingestion module to work as a Tier 1 operator'
		
		_alert = ingestor.main(True, True)
		if not (_alert):
			print('\t - No Security Incident found in this ticket.')
			print '\n-------------------------------\n'				
			return
		_exa_handle_alert (_alert)

def handle_incident_t2(tickets):
	'''
	expects array of ticket:subject as input, the output of [rt_find_new_tickets | rt_find_ticket_byID
	'''
	for ticket in tickets:
		try:
			tid = ticket.split(':')[0].strip()
			tsubject = ticket.split(':')[1].strip()
			ticket_props = rtracker.rt_get_ticket_prop(tid)		
			tQ = ticket_props[1].split(':')[1].strip()
			tCreator = ticket_props[3].split(':')[1].strip()
			tPriority = ticket_props[6].split(':')[1].strip()
			
			ioc = []
			##	  0			1		   2			3			4
			## CF_inv_acc, tCF_inv_app, tCF_inv_sip, tCF_inv_dip, tCF_inc_sig
			for i in range(23, 29):
				try:
					ioc.append(None if ticket_props[i].split(':',1)[1].strip() == '' else repr(ticket_props[i].split(':',1)[1].strip()).strip("'")) ##strip the single quote with repr to get the raw ioc
				except:
					continue
			print('Ticket #%s: [%s] assigend by [%s]') %( colo.format(tid, 'OKGREEN'), colo.format(tsubject, 'OKGREEN'), colo.format(tCreator, 'OKGREEN'))
			#print ('\t - %s' %ioc)
			history = rtracker.rt_find_IOCs(tid, tQ, ioc)

			if history:
				print ('\t - Searching tickets history:')
				for k, v in enumerate(history):
					print "\t\t - %s appeared before in tickets %s" %(colo.format(v[0], 'BOLD'), colo.format(v[1], 'BOLD'))
					#found_tid = []
					#for tkt in v[1]:
					#	if tkt.split(':')[0] == tid: continue
					#	found_tid.append(tkt.split(':')[0])
					#if found_tid:
					#	print "\t\t - %s appeared before in %s" %(v[0], found_tid)
			else:
				print ('\t - No history for this ticket.')				
			_alert = rtracker.rt_get_alert_from_ticket(tid)
			if not (_alert):
				print('\t - No Security Incident found in this ticket.')
				print '\n-------------------------------\n'
				continue
			_exa_handle_alert (_alert)
			print '\n-------------------------------\n'
		except Exception as e:
			print 'Error\n%s' %str(e)
			print(traceback.format_exc())
						
def _test_exa_handle_alert():
	y = []
	for alert_lnk in y:
		_exa_handle_alert(alert_lnk)
		print '\n-------------------------------\n'

def _compose_IR_email(alert_time, alert_name, alert_kvalue, data):
	_txt = 'Hi,\n'
	_err = False
	try:
		_txt += 'On date %s, SIEM has registered a possible incident regarding [%s]\n' %(alert_time, alert_kvalue)
		_txt += '\tAlert: %s\n' %alert_name.strip('BS ').strip(' All')
		for k, v in (data.iteritems()):
			if k =='Status' and v == 'Failed':
				_txt = '\033[91m'
				_err = True
			if v is not None:
				_txt += '\t%s: %s\n' %(k, v)
	except:
		#print ('\n*\n%s\n*\n') %data
		_txt += data
	_txt += '\nBest regards,\nZen SOC Team' if not _err else '\033[0m'
	return _txt

def _exa_handle_alert(alerts):
	for alert_lnk in alerts:
		try:
			#print ('\nHandling link #%s:\n\n' %alert_lnk)
			results = exabeam.exa_get_alert_data(alert_lnk)
			if results:
				key_value  = results[1]
				log_docs   = results[2]
				alert_name = results[3]
				alert_sev  = results[4]
				alert_time = results[5]
				
				res = exabeam.exa_get_log_data(log_docs)
				if res and res[0] == 1:
					print '\t - There %s for the %s [%s] concerning [%s]:\n' %('are %s events' %colo.format(str(res[2]), 'WARNING') if res[2] > 1 else 'is %s event' %colo.format(str(res[2]), 'WARNING'), colo.format(alert_sev, 'WARNING'),alert_name, colo.format(key_value, 'WARNING'))
					i = 1
					for evt in res[4]:
						email = _compose_IR_email(alert_time, alert_name, key_value, analyzer.handle_event(evt))
						print colo.format(('Handling Event: #%d\n------------------\n' %i), 'BOLD')
						print ('{:>20}'.format(email))
						print ('{:>20}'.format('**\n%s\n**\n' %evt))
						i += 1
						#
				elif res[0] == -1:
					print ('Error handling [%s - %s]: Unable to parse URL' %(alert_name, alert_sev))
				elif res[0] == 0:
					print ('Error handling [%s - %s]: No Results' %(alert_name, alert_sev))
				else:
					#print ('[%s - %s]: %s' %(alert_name, alert_sev, res[1]))
					print('operation failed')
		except:
			print(traceback.format_exc())

def __init():
	global ctx, rt_cj, rt_loader, tier_one, colo, VERBOSE, DIAGNOSE, args
	colo = bcolors()
	
	args = _argsparse()
	tier_one = True if args.mode == 1 else False
	VERBOSE = True if args.verbose else False
	DIAGNOSE = args.diagnose if args.diagnose else False
	
	if not os.path.isfile('cacerts.cer'):
		print (colo.format('ERROR #1-101: Can not establish a secure channel', 'FAIL') )
		return False
	
	ctx = ssl.create_default_context(cafile = 'cacerts.cer')
	rt_cj = cookielib.LWPCookieJar()
	rt_loader = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx), urllib2.HTTPCookieProcessor(rt_cj))
	return True

def _argsparse():
	
	parser = argparse.ArgumentParser(description="S4-IR v0.1 by Sleiman Ahmad")
	group = parser.add_mutually_exclusive_group()
	group.add_argument("-t", "--ticket", type=int, help="handle a specefic ticket (RT)")
	group.add_argument("-l", "--link", help="handle a specefic link (Exabeam)")
	group.add_argument("-d", "--diagnose",  help="handle a specefic ticket or link")
	parser.add_argument("-m", "--mode", type=int, choices=[1, 2], default=2, help="operation mode, tier 1 or tier 2")
	parser.add_argument("-v", "--verbose", action="store_true", help="increase verbosity (Error debugging)")
	parser.add_argument("-u", "--user", help="search user behaviour history ")
	return parser.parse_args()
	
def usage():
	print ("\n***************************\nS4-IR v0.1 by Sleiman Ahmad\n***************************\n")
	print ('Usage: %s [-m|--mode T1|T2]' %os.path.basename(__file__))
	print ('Example: %s -m T1\n' %os.path.basename(__file__))

def sleep(u, p, auth, mode = 2):
	global access_user, access_password, tier_one
	if mode == 1: 
		tier_one = True
	if u != '' and p != '':
		access_user = u
		access_password = p
		if not helper.is_base64(p):
			p = p.decode('base64')

	if (__init()):
		main()

if __name__ == '__main__':
	if (__init()):
		main()
	