#!/usr/bin/env python2.7

### S4
### Author: Sleiman Ahmad
### April 2019

#Authentication
USER = ''
PASS = ''
AUTH = ''

SSL_CERTIFICATE = ''

#ingestion - Fetch/Pull
REMOTE_HOST = ''
REMOTE_PORT = 0
INGEST_AUTH = ''
REMTOE_PATH = ''

#UBA
AA_NOTABLE_RISK_SCORE = 0
AA_SEARCH_PAST_X_DAYS = 0


#ADVANCED SETTINGS - YOU CAN BREAK THINGS HERE, DON'T CHANGE THOSE VALUES UNLESS YOU KNOW WHAT YOU ARE DOING

#events handler
ST_LOGS_PAN = ''
ST_LOGS_FGT = ''
ST_LOGS_TM  = ''

#ingestion
ING_FILTER = ['']
EXA_RULE_CAT_FILTER = ['IT OPS']

import sys
def __init():
	sys.path.insert(1, 'utils/')
	return True
def main():
	try:
		import s4
		s4.sleep(USER, PASS, AUTH)
	except ImportError as e:
		print ('Some files are missing!')
		exit(-1)
if __name__ == '__main__':
	if (__init()):
		main()