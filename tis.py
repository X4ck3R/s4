from tinydb import TinyDB, Query
import os 
#!/usr/bin/env python2.7

__version__ = '0.103'

DB_NAME = '/s4.db'
db = None


def _getContextTable(alert_name):
	if alert_name.startswith('EMEA'):
		return db.table('EMEA')
	return db.table('BS')

def _getIOCHistory(tbl, key, val):
	query = Query()
	if (val is None) or (not val) or (val == ''):
		return
	res_tickets = []	
	res = tbl.search((query[key] == val))
	if len(res) > 0:
		for r in res:
			res_tickets.append(r['rt-ticket'])
		return [key, val, set(res_tickets)]
	return

def searchPastTickets(alert_name, data, isComment = False):
	'''
	return array of founds: [[KEY, VAL, {}], [KEY, VAL, {}]]
	'''
	global db
	try:
		db = TinyDB(os.getcwd() + DB_NAME)
	except Exception as e:
		print 'could not connect to db %s' %str(e)
		return []
		
	tbl = _getContextTable(alert_name)
	IOC = ['User', 'Source-IP', 'Destination-IP', 'Threat-Name', 'Service', 'File-Name', 'Attack/Virus', 'Source-Host', 'Destination-Host', 'File-Hash', 'Process', 'Virus-Name']
	BLACK_LIST = ['chrome.exe', 'firefox.exe']
	founds = []
	if isComment:
		## in comment the data type is a set 
		for k, v in (data):
			if k in IOC:
				r = _getIOCHistory(tbl, k, v)
				if r:
					founds.append(r)
		return founds	
	else:
		## in ticket the data type is a dic
		for k, v in (data.iteritems()):
			if k in IOC:
				r = _getIOCHistory(tbl, k, v)
				if r:
					founds.append(r)
		return founds

def getVersion():
	return __version__

##########
## TEST ##
########## 

