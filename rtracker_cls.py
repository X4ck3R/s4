#!/usr/bin/env python2.7

## Author: Sleiman Ahmad
## Email: Sleiman.xsp@gmail.com

import cookielib, urllib, urllib2, ssl, json, os, re, traceback, datetime, inspect, helper
from dateutil.parser import parse


colo = helper.bcolors()
class RequestTracker:
	RT_BASE_URI = 'https://check-mk.platform.nttzen.cloud:8443/REST/1.0/'
	RT_CA_CRT = 'cacerts.cer'
	RES_OK = '200 Ok'
	VERBOSE = True
	url_opener = ctx = None

	def __init__(self, user, password, cacert=None):
		if cacert is not None: RequestTracker.RT_CA_CRT = cacert
		RequestTracker.ctx = ssl.create_default_context(cafile = RequestTracker.RT_CA_CRT)
		self.cj = cookielib.LWPCookieJar()
		RequestTracker.url_opener = urllib2.build_opener(urllib2.HTTPSHandler(context=RequestTracker.ctx), urllib2.HTTPCookieProcessor(self.cj))
		self.access_user = user
		self.access_password = password		

	def rt_login(self):
		'''
		login to RT. password should be base64 encoded.
		'''
		try:
			rt_auth_data = urllib.urlencode({'user': self.access_user, 'pass': self.access_password.decode('base64')})
			res = self.url_opener.open(RequestTracker.RT_BASE_URI, rt_auth_data).read()
			self.__print(res)
			return True if RequestTracker.RES_OK in res else False
		except:
			self.__print(traceback.format_exc())
			print('RT#501: Not able to login')
			return False

	def rt_get_ticket_prop(self, tid):
		'''
		Get Tikcet Props. returns multilines 
		'''
		try:
			uri = RequestTracker.RT_BASE_URI + 'ticket/%s/show' %tid
			res = self.url_opener.open(uri).read().split('\n')
			self.__print(res)
			return res[2:-2] if (RequestTracker.RES_OK in res[0] and 'No ' not in res[2]) else False
		except:
			self.__print(traceback.format_exc())
			print('RT#502: Error retrieve props')
			return False

	def __rt_list_ticket_attach(self, tid):
		uri = RequestTracker.RT_BASE_URI +'ticket/%s/attachments/' %tid
		res = self.url_opener.open(uri).read()
		self.__print(res)
		return res if RequestTracker.RES_OK in res else False

	def __rt_read_ticket(self, tid):
		attid = self.__rt_get_ticket_attid(tid)
		if not attid:
			return False
		uri = RequestTracker.RT_BASE_URI + 'ticket/%s/attachments/%s' %(tid, attid)
		res = self.url_opener.open(uri).read()
		self.__print(res)
		return res if RequestTracker.RES_OK in res else False
		
	def __rt_get_ticket_attid(self, tid):
		try:
			for x in self.__rt_list_ticket_attach(tid).split('\n'):
				if 'text/html' in x:
					return (x.split(':')[0] if (x.split(':')[0].strip()).isdigit() else x.split(':')[1]).strip()
		except Exception as e:
			self.__print(traceback.format_exc())
			print ('RT#503: error getting attid')
			return False

	def rt_get_alert_from_ticket(self, tid):
		tkt = self.__rt_read_ticket(tid)
		try:
			links = re.findall('rt: <a href="(.*)">', tkt)
			if links is not None and len(links) > 0:
				return links
			return False
		except Exception as e:
			self.__print(traceback.format_exc())
			print ('RT#504: Error getting alerts')
			return False

	def rt_find_new_tickets(self, _usr):
		qry = "query=Owner='%s'AND(Status='new')" %(_usr)
		return self.__rt_search(qry)

	def rt_find_ticket_byID(self, _id):
		qry = "query=id='%s'" %(_id)
		return self.__rt_search(qry)

	def rt_find_IOCs(self, tid, queue, iocs):
		self.__print(iocs)
		##	  0			1		   2			3			4
		## CF_inv_acc, tCF_inv_app, tCF_inv_sip, tCF_inv_dip, tCF_inc_sig
		q = "query=Queue='%s'" %queue
		def __get_clause(idx, val):
			self.__print(val)
			clauses = [" AND 'CF.{Incident Involved Accounts}' = '%s'" %val,
		" AND 'CF.{Incident Involved Applications}' = '%s'" %val,
		" AND 'CF.{ 	Incident Source IP addresses}' = '%s'" %val,
		" AND 'CF.{Incident Target IP addresses}' = '%s'" %val,
		" AND 'CF.{Signature Incident}' = '%s'" %val,
		" AND 'CF.{Signature Incident ID}' = '%s'" %val
		]
			#exception_clause " OR 'CF.{Incident Involved Accounts}' = '%s'" %val,
			return clauses[idx]
		## check if two IOCs in one filed separated by ,
		found = []
		for idx, ioc in enumerate(iocs):
			if ioc is not None:
				ioc = ioc.split(',')[0] if ',' in ioc and idx == 0 else ioc
				qry = q + __get_clause(idx, ioc)
				self.__print ('handling query: %s\n' %qry)
				res =  self.__rt_search(qry)
				if not res or (len (res) == 1 and res[0].split(':')[0] == tid): continue
				found.append([ioc, [_id.split(':')[0] for _id in res]])
		return found if found else False
		
	def __rt_search(self, qry):
		uri = RequestTracker.RT_BASE_URI + 'search/ticket?%s' %urllib.quote(qry, safe ='=/\\')
		self.__print ('uri: %s\n' %uri)
		request = urllib2.Request(uri)
		request.add_header('Accept','*/*')
		request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0')
		request.add_header('Referer', 'https://check-mk.platform.nttzen.cloud:8443/REST/1.0/')
		res = self.url_opener.open(request).read().split('\n')
		self.__print (res)
		return res[2:-3] if ('200 Ok' in res[0] and 'No ' not in res[2]) else False

	def __print(self, msg):
		if RequestTracker.VERBOSE:
			_caller = inspect.stack()[1][3]
			str = "%s - [%s] - %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), colo.format(_caller, 'WARNING'), msg)
			print (str)