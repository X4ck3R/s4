#!/usr/bin/env python2.7


### S4'IR
##{* Sleiman's Smart Sleeping SOAR *}###
### Author: Sleiman Ahmad
### v0.172
###


import cookielib, urllib, urllib2, ssl, json, os, re, traceback, datetime, sys


def main():

	def __rt_build_comment(tid, body):
		tkt_num =tid
		tkt_body = body
		try:
			uri = RT_BASE_URI + 'ticket/%s/comment' %tkt_num
			tkt = 'id: %s\nAction: comment\nContent-Type: text/html; charset="UTF-8"\nText: %s' %(tkt_num, tkt_body)
			data = {'content':tkt}
			rt_tkt_data = urllib.urlencode(data)
			res = url_opener.open(uri, rt_tkt_data).read()
			print res
			if RES_OK in res:
				print ('ok')
			return False
		except:
			print(traceback.format_exc())
			print('RT#502-0: Error edit ticket')
			return False
	access_user = 'Salomone'
	access_password = 'MTMyMTMy'

	RT_BASE_URI = 'http://192.168.128.10/REST/1.0/'
	
	RES_OK = '200 Ok'
	
	cj = cookielib.LWPCookieJar()
	handlers = [urllib2.HTTPHandler(), urllib2.HTTPCookieProcessor(cj)]
	url_opener = urllib2.build_opener(*handlers)

	rt_auth_data = urllib.urlencode({'user': access_user, 'pass': access_password.decode('base64')})
	res = url_opener.open(RT_BASE_URI, rt_auth_data).read()
	print res
	
	# for c in cj:
		# print c.name, c.value
	
	#print url_opener.open('http://192.168.128.10/REST/1.0/ticket/5000/show').read()

	comment = "Hi,\nOn date 23 April 2020 10:39:28 GMT, SIEM has registered a possible incident regarding [172.20.6.88]\n\tAlert: EMEA Malicious Activity Internal PAN\n\tD-Port: 445\n\tUser: ebs\alexandru.mihut\n\nBest regards,\nZen SOC Team"
	comment = comment.replace('\n','<br />').replace('\t','        ')

	tkt = rt_get_ticketPost_PAN_MAL_ALL('Trash-Test', 'EMEA Malicious Activity Internal PAN All', comment, feed_PAN_ALL())
	data = {'content':tkt}
	uri = RT_BASE_URI + 'ticket/new'
	rt_tkt_data = urllib.urlencode(data)
	res = url_opener.open(uri, rt_tkt_data).read()
	if RES_OK in res:
		tid = re.findall('Ticket (\d+) created', res)
		__rt_build_comment(tid[0], comment)
		#__rt_register_ticket(tid, assignee)



def feed_PAN_ALL():
	a = {'D-Port': '445', 'Severity': 'medium', 'Service': 'ms-ds-smbv3', 'File-Name': u'', 'Source-IP': '10.224.13.14', 'Threat-Name': 'Virus/Win32.WGeneric.ajbbrx(340828689)', 'Action': 'reset-both', 'Event-Time': '2020/04/14 15:08:57', 'Direction': 'server-to-client', 'Type/Subtype': 'THREAT/virus', 'S-Port': '49804', 'Destination-IP': '10.224.3.150', 'User': 'ebs\\bogdan.tocila'}
	return a

def rt_get_ticketPost_PAN_MAL_ALL(tkt_q, tkt_subject, tkt_body, data):
	#data = json.loads(data)
	queue = tkt_q #'Trash-Test'
	assignee = __get_available_assignee()
	body = tkt_body
	subj = tkt_subject #'EMEA Malicious Activity Internal PAN All'	
	accounts = data['User']
	apps = data['Service']
	src_ip = data['Source-IP']
	dst_ip = data['Destination-IP']
	sig = data['Threat-Name']
	sig_id = sig[sig.find("(")+1:sig.find(")")]
	occ_time = data['Event-Time']
	return __rt_build_ticket(queue, assignee, subj, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
	


def __get_available_assignee(list_Tier2=None):
	##working hours:
	list_Tier2 = ['ahmads', 'ahmads', 'ahmads', 'ahmads']
	return list_Tier2[0]

def __rt_register_ticket(id, owner):
	'''
	register every ticket to the disk
	in: tid, towner, length of assignee list
	'''
	with open('register.out', 'a+') as register:
		register.write('timestamp - %s - %s' %(id, owner))


def rt_open_ticket(self, data):
	'''
	Open a ticket.
	'''	
	try:
		uri = self.RT_BASE_URI + 'ticket/new'
		tkt = self.__rt_build_ticket(queue, assignee, subj, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
		data = {'content':tkt}
		rt_tkt_data = urllib.urlencode(data)
		res = self.url_opener.open(uri, rt_tkt_data).read()
		if RES_OK in res:
			return re.findall(r'\d+', res.splitlines()[2])[0]
		return False
	except:
		print(traceback.format_exc())
		print('RT#502-0: Error open ticket')
		return False

def ticket_edit(tkt_num, tkt_body):

	try:
		uri = self.RT_BASE_URI + 'ticket/%s/edit' %str(tkt_num)
		tkt = '''Text: %s''' %tkt_body
		data = {'content':tkt}
		rt_tkt_data = urllib.urlencode(data)
		res = self.url_opener.open(uri, rt_tkt_data).read()
		if RES_OK in res:
			return re.findall(r'\d+', res.splitlines()[2])[0]
		return False
	except:
		print(traceback.format_exc())
		print('RT#502-0: Error edit ticket')
		return False

def __rt_build_ticket(Q, assignee, subj, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time):
	x = '''
id: ticket/new
Queue: %s
Owner: %s
Subject: %s
Status: new
Priority: 1
InitialPriority: 0
FinalPriority: 0
Requestors: salomone@nttdata.com
Cc: sleiman.ahmad@nttdata.com
AdminCc:
TimeEstimated: 0
Content-Type: text/html; charset="UTF-8"
Text: %s
CF.{Client Incident ID}:
CF.{Case Incident ID}:
CF.{Incident Involved Accounts}: %s
CF.{Incident Involved Applications}: %s
CF.{Incident Source IP addresses}: %s
CF.{Incident Target IP addresses}: %s
CF.{Signature Incident}: %s
CF.{Signature Incident ID}: %s
CF.{Tier1_working_time_elapsed}:
CF.{Tier2_working_time_elapsed  }:
CF.{Incident Occurence Time}: %s
CF.{Role}: 1
'''
	return x %(Q, assignee, subj, body, accounts, apps, src_ip, dst_ip, sig, sig_id, occ_time)
	

def __init():
	return True


if __name__ == '__main__':
	if (__init()):
		main()
	