#!/usr/bin/env python2.7

## Author: Sleiman Ahmad
## Email: Sleiman.xsp@gmail.com
## version 1.152

import cookielib, urllib, urllib2, ssl, json, os, re, traceback, datetime, inspect, helper
from dateutil.parser import parse
from datetime import date, timedelta

colo = helper.bcolors()
__version__ = '1.270'
class Exabeam:

	EXABEAM_DL_URI = 'https://ssi-em-dl01.emea-bs.nttzen.cloud:8484/'
	EXABEAM_AA_URI = 'https://sue-em-aa01.emea-bs.nttzen.cloud:8484/'
	EXABEAM_CA_CRT = 'cacerts.cer'
	VERBOSE = True
	NOTABLE_RISK_SCORE = 20
	url_opener = aa_url_opener = ctx = None
	doc_idx = None
	
	def __init__ (self, user, password, cacert=None ):
		#global access_user, access_password, ctx = cj
		if cacert is not None: Exabeam.EXABEAM_CA_CRT = cacert
		Exabeam.ctx = ssl.create_default_context(cafile = Exabeam.EXABEAM_CA_CRT)
		self.cj = cookielib.LWPCookieJar()
		self.aa_cj = cookielib.LWPCookieJar()
		Exabeam.url_opener = urllib2.build_opener(urllib2.HTTPSHandler(context=Exabeam.ctx), urllib2.HTTPCookieProcessor(self.cj))
		Exabeam.aa_url_opener = urllib2.build_opener(urllib2.HTTPSHandler(context=Exabeam.ctx), urllib2.HTTPCookieProcessor(self.aa_cj))
		self.access_user = user
		self.access_password = password

	def exa_aa_login(self):
		'''
		login to AA.
		'''
		try:
			self.__print ('AA login attempt for user %s\n' %(self.access_user))
			exabeam_uri = Exabeam.EXABEAM_AA_URI + 'api/auth/login'
			exabeam_auth_data = urllib.urlencode({'username': self.access_user, 'password': self.access_password.decode('base64')})
			return Exabeam.aa_url_opener.open(exabeam_uri, exabeam_auth_data).read()
		except urllib2.HTTPError, error:
			print 'Error #%s - %s' %(error.code, error.reason)
			self.__print(traceback.format_exc())
			return False
		except:
			print ('Error in logging in')
			self.__print(traceback.format_exc())
			return False

	def exa_aa_request_user_info(self, user):
		exabeam_uri = Exabeam.EXABEAM_AA_URI + 'uba/api/user/%s/info'%(user)
		res = {}
		self.__print ('requesting user info for %s' %(user))
		_res = self.__exa_request(exabeam_uri, mode='AA')
		self.__print ('response:\n%s\n' %(_res))

		def _validate(x, y, res):
			return y[x] if x in y else 'N/A' 
		
		data = json.loads(_res)
		if not 'userInfo' in data:
			res['info'] = 'no information for this user'
			return res
		try:
			_info = data['userInfo']
			res['username']  = _info['username']
			res['userscore'] = _info['riskScore']
			res['pastscores'] = _info['pastScores']
			res['lastactive'] = self.__from_epo(_info['lastActivityTime'])
			res['name'] = _validate('fullName', _info['info'], res)
			res['city'] = _validate('location', _info['info'], res)
			res['job'] = '%s - %s' %(_validate('division',_info['info'], res),_validate('department',_info['info'], res) )
			res['title'] = _validate('title', _info['info'], res)
			return res
			
		#except KeyError:
		#	pass
		#	return res
		except Exception as e:
			print colo.format(traceback.format_exc(), 'FAIL')

	def exa_aa_request_user_history(self, user, ndays=10):
		'''
		Get user profile in 30 days.
		'''
		end_time = self.__to_epo(datetime.datetime.now())
		start_time = self.__to_epo(datetime.datetime.now() - timedelta(days=ndays))
		
		exabeam_uri = Exabeam.EXABEAM_AA_URI + 'uba/api/user/%s/sequences?username=%s&startTime=%s&endTime=%s'%(user, user, start_time, end_time)
		self.__print ('requesting UBA for %s in the past %d days, request:\n%s\n\n' %(user, ndays, exabeam_uri))
		
		_seq = self.__exa_request(exabeam_uri, mode='AA')
		_sessions = json.loads(_seq)['sessions']
		_s_ids = _s_dates = _s_score = []
		_data = {}
		_d = []
		for _session in _sessions:
			_s_trg_rules = []
			if _session['riskScore'] >= Exabeam.NOTABLE_RISK_SCORE:
				if _session['sessionId']:
					_s_id = _session['sessionId']
				else:
					self.__print ('No session ID found')
					continue
				
				#_s_ids.append(_session['sessionId'])
				#_s_dates.append(self.__from_epo(_session['endTime']))
				_data['date'] = self.__from_epo(_session['startTime'])
				_data['score'] = _session['riskScore']
				
				try:
					_rules = (self.__exa_aa_get_session(_s_id))
					if _rules:
						for _rule in _rules:
							if _rule['ruleId'] not in _s_trg_rules: _s_trg_rules.append(_rule['ruleId'])
						_data['rules'] = _s_trg_rules
				except Exception as e:
					#print e
					continue
				##print _data
				##print '\n\n'
				_d.append(json.dumps(_data))
			else:
				self.__print ('%s is not a notable score' %(_session['riskScore']))

		return _d if _d else False

	def exa_aa_request_alerts_by_asset(self, asset):
		
		exabeam_uri = Exabeam.EXABEAM_AA_URI + 'uba/api/asset/%s/securityAlerts?_=%d&sortBy=date&sortOrder=-1&numberOfResults=101' %(asset.lower(), self.__to_epo(datetime.datetime.now()))
		self.__print ('request:\n%s\n' %exabeam_uri)
		res = self.__exa_request(exabeam_uri, mode='AA')
		self.__print ('response:\n%s\n' %res)
		return json.loads(res)
	
	def __exa_aa_get_session(self, sid):
		self.__print ('requesting session info for %s :' %(sid))
		exabeam_uri = Exabeam.EXABEAM_AA_URI + 'uba/api/session/%s/info' %sid
		res = self.__exa_request(exabeam_uri, mode='AA')
		return json.loads(res)['triggeredRules']

	def exa_dl_login(self):
		'''
		login to DL. password should be base64 encoded.
		'''
		self.__print ('DL login attempt for user %s\n' %(self.access_user))
		try:
			exabeam_uri = Exabeam.EXABEAM_DL_URI + 'api/auth/login'
			exabeam_auth_data = urllib.urlencode({'username': self.access_user, 'password': self.access_password.decode('base64')})
			return Exabeam.url_opener.open(exabeam_uri, exabeam_auth_data).read()
		except urllib2.HTTPError, error:
			print 'Error #%s - %s' %(error.code, error.reason)
			self.__print(traceback.format_exc())
			return False
		except:
			print ('Error in logging in')
			self.__print(traceback.format_exc())
			return False

	def __exa_alert_to_searchable(self, lnk):
		'''
		prepare query to DL. input is an alert uri coming from a DL-CR-Syslog or a RT-Ticket, 
		'''
		if not 'exabeam-' in lnk:
			self.__print('not a valid link: %s' %lnk)
			return False
		try:
			if 'logs?id=' in lnk:
			##link from email: 
				self.__print('alert link from email')
				doc = lnk.split("/")[8]
				
				ids = lnk.split("/")[9].split("=")[1] if '&' not in lnk else lnk.split("/")[9].split("=")[1].split('&')[0]
			else:
				##link from syslog
				self.__print('alert link from syslog')
				doc = lnk.split("/")[3]
				ids = lnk.split("/")[5]
			
			# store the idx to fix bugs in __exa_log_to_searchable() when start and stop are in two different days
			Exabeam.doc_idx = doc
			
			res = {"query":{"ids":{"type":"logs","values":[ids]}},"stored_fields":["*"],"_source":True,"script_fields":{},"docvalue_fields":["original_index_time","indexTime","@timestamp","exa_rawEventTime","exa_adjustedEventTime","time","timestamp"]}
			#__print()
			return doc, res
		except:
			print(traceback.format_exc())
			return False

	def __exa_request_alert_data(self, lnk):
		'''
		query to DL for an alert document
		'''
		doc, query = self.__exa_alert_to_searchable(lnk)
		self.__print ('requesting for document: %s and query:\n**\n%s\n**\n' %(doc, query))
		exabeam_uri = Exabeam.EXABEAM_DL_URI + 'data/elasticsearch/%s/_search' %(doc)
		return self.__exa_request(exabeam_uri, data=query)

	def exa_get_alert_data(self, lnk):
		'''
		input is json response of request_alert_data
		'''
		alert_obj = self.__exa_request_alert_data(lnk)
		self.__print ('alert response is:\n**\n%s\n**\n' %(alert_obj))
		## todo: check exa_rule_category
		##
		##
		try:
			d = json.loads(alert_obj)
			took = d['took']
			qry_kv = d['hits']['hits'][0]['_source']['query_key_value']
			log_lnk = d['hits']['hits'][0]['_source']['exa_link_logs']
			alert_name = d['hits']['hits'][0]['_source']['alert_name']
			alert_severity = d['hits']['hits'][0]['_source']['alert_severity']
			alert_time = d['hits']['hits'][0]['_source']['indexTime']
			exa_rule_category = d['hits']['hits'][0]['_source']['exa_rule_category']
			exa_rule_name = d['hits']['hits'][0]['_source']['exa_rule_name']
			exa_alert_desc = d['hits']['hits'][0]['_source']['exa_rule_description']
			original_index_time = d['hits']['hits'][0]['_source']['original_index_time']
			#######   0     1           2           3            4				5													              6                           7	    				8                9
			return took, qry_kv, log_lnk, alert_name, alert_severity, alert_time, exa_rule_category, exa_rule_name, exa_alert_desc, original_index_time
		except ValueError, e:
			print(traceback.format_exc())
			print(lnk)
			return False

	def __exa_log_to_searchable(self, lnk):
		''' prepare log query to be sent to DL. input is the log uri coming from an alert document or RT-Ticket
		'''
		self.__print('preparing request for log data on link:\n**\n%s\n**\n' %lnk)
		try:
			_from = re.search("from:'(.*)',mo", lnk).group(1)
			_idx = Exabeam.doc_idx if Exabeam.doc_idx is not None else 'exabeam-%s' %(_from.split('T')[0].replace('-','.'))
			_from = self.__to_epo(parse(_from))
			_to = self.__to_epo(parse(re.search("to:'(.*)'\)\)&", lnk).group(1)))
			_qry = '%s' %(re.search("e,query:'(.*)'", lnk).group(1)).replace('+', ' ').replace('%22', '"').replace('%3A', ':').replace('%3D', '=').replace('%2F','/').replace('%28','(').replace('%29',')')
			##
			## before DLi32
			##
			_prf = int(datetime.datetime.now().strftime("%s")) * 1000
			exa_q1 = {"index":[_idx],"ignore_unavailable":True,"preference":_prf}
			exa_q2 = {"size":5000,"sort":[{"indexTime":{"order":"desc","unmapped_type":"boolean"}}],"highlight":{"pre_tags":[""],"post_tags":[""],"fields":{"*":{}},"require_field_match":False,"fragment_size":2147483647},"stored_fields":["*"],"_source":True,"script_fields":{},"docvalue_fields":["original_index_time","indexTime","@timestamp","exa_rawEventTime","exa_adjustedEventTime","time"],"query":{"bool":{"must":{"bool":{"must":[{"query_string":{"analyze_wildcard":True,"default_field":"message","query":_qry}},{"range":{"indexTime":{"gte":_from,"lte":_to,"format":"epoch_millis"}}}],"must_not":[]}},"filter":{"bool":{"should":[],"minimum_should_match":1,"must_not":[]}}}}} 
			query = '%s\n%s\n' %(json.dumps(exa_q1), json.dumps(exa_q2))
			##
			## DLi32-10 and later
			##
			exa_DLi32_q = {"clusterWithIndices":[{"clusterName":"local","indices":[_idx]}],"query":_qry,"docValues":["indexTime"],"source":True,"queryAnalyzeWildcard":True,"queryDefaultField":"message","storedFields":["*"],"highlight":True,"size":500,"sortBy":[{"field":"indexTime","order":"desc"}],"rangeQuery":{"field":"indexTime","lte":_to,"gte":_from},"dateHistogramAggr":{"field":"indexTime","interval":"5s","timeZone":"Europe/Berlin"}}
			query = json.dumps(exa_DLi32_q)
			##
			return query
		except:
			print(traceback.format_exc())
			return False

	def exa_dl_craft_query(self, _from, _to, _q):
		_q = _q.replace('+', ' ').replace('%22', '"').replace('%3A', ':').replace('%3D', '=').replace('%2F','/').replace('%28','(').replace('%29',')')
		_idx = 'exabeam-%s' %(datetime.date.today().strftime("%Y.%m.%d"))
		_prf = self.__to_epo(datetime.datetime.now())
		exa_q1 = {"index":[_idx],"ignore_unavailable":True,"preference":_prf}
		exa_q2 = {"size":5000,"sort":[{"indexTime":{"order":"desc","unmapped_type":"boolean"}}],"highlight":{"pre_tags":[""],"post_tags":[""],"fields":{"*":{}},"require_field_match":False,"fragment_size":2147483647},"stored_fields":["*"],"_source":{"excludes":["message"]},"script_fields":{},"docvalue_fields":["original_index_time","indexTime","@timestamp","exa_rawEventTime","exa_adjustedEventTime","time"],"query":{"bool":{"must":{"bool":{"must":[{"query_string":{"analyze_wildcard":True,"default_field":"message","query":_q}},{"range":{"indexTime":{"gte":_from,"lte":_to,"format":"epoch_millis"}}}],"must_not":[]}},"filter":{"bool":{"should":[],"minimum_should_match":1,"must_not":[]}}}}} 
		query = '%s\n%s\n' %(json.dumps(exa_q1), json.dumps(exa_q2))
		self.__print('requesting the query:\n**\n%s\n**\n' %query)
		try:
			exabeam_uri = Exabeam.EXABEAM_DL_URI + 'data/elasticsearch/_msearch'
			request = urllib2.Request(exabeam_uri)
			request.add_header('Content-Type', 'application/x-ldjson')
			request.add_header('Accept','*/*')
			request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0')
			request.add_header('Referer', 'https://ssi-em-dl01.emea-bs.nttzen.cloud:8484/data/app/dataui')
			request.add_header('kbn-version', '5.1.1-SNAPSHOT')
			request.add_header('Cookie', self.__get_cookie(self.cj))
			_res = urllib2.urlopen(request, query, context=Exabeam.ctx).read()
		except:
			print(traceback.format_exc())
			return False	
		return self.__exa_dl_handle_qresponse(_res)

	def __exa_dl_handle_qresponse(self, response):
		data_obj = response
		self.__print('log data response is:\n%s' %data_obj)
		try:
			d = json.loads(data_obj)
			if d['responses'][0]['status'] == 400:
				_err_type = d['responses'][0]['error']['type']
				_err_reason = d['responses'][0]['error']['failed_shards'][0]['reason']['caused_by']['reason']
				return [-1, 'Error in the query. type: [%s] - reason: [%s]' %(_err_type, _err_reason)]
			if d['responses'][0]['status'] == 200:
				_took = d['responses'][0]['took']
				_nhit = d['responses'][0]['hits']['total']
				_hits = d['responses'][0]['hits']['hits']
				self.__print('took: %d, nhits: %d' %(_took, _nhit))
				_msgs = []
				if _nhit > 0:
					res = 1
					for hit in _hits:
						_msgs.append(hit['highlight']['message'][0])
					return res, _took, _nhit, _hits, _msgs
				return [0,'no results found in the response :\n%s\n' %d]
		except ValueError, e:
			print(traceback.format_exc())
			return False

	def __exa_request_log_data(self, lnk):
		query = self.__exa_log_to_searchable(lnk)
		self.__print('requesting the query:\n**\n%s\n**\n' %query)
		try:
			## USE 'data/elasticsearch/_msearch' for < DLi32
			exabeam_uri = Exabeam.EXABEAM_DL_URI + 'dl/api/es/search' 
			request = urllib2.Request(exabeam_uri)
			## USE 'application/x-ldjson' for < DLi32
			request.add_header('Content-Type', 'application/json;charset=utf-8')
			request.add_header('Origin', 'https://ssi-em-dl01.emea-bs.nttzen.cloud:8484')
			request.add_header('Accept','application/json, text/plain, */*')
			request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0')
			request.add_header('Referer', 'https://ssi-em-dl01.emea-bs.nttzen.cloud:8484/data/app/dataui')
			request.add_header('kbn-version', '5.1.1-SNAPSHOT')
			request.add_header('Cookie', self.__get_cookie(self.cj))
			return urllib2.urlopen(request, query, context=Exabeam.ctx).read()
		except:
			print(traceback.format_exc())
			return False
	
	def exa_get_log_data(self, lnk):
		'''
		input is json response of request_log_data
		'''
		data_obj = self.__exa_request_log_data(lnk)
		self.__print('log data response is:\n%s' %data_obj)
		try:
			d = json.loads(data_obj)
			if d['responses'][0]['status'] == 400:
				_err_type = d['responses'][0]['error']['type']
				_err_reason = d['responses'][0]['error']['failed_shards'][0]['reason']['caused_by']['reason']
				return [-1, 'Error in the query. type: [%s] - reason: [%s]' %(_err_type, _err_reason)]
			if d['responses'][0]['status'] == 200:
				_took = d['responses'][0]['took']
				_nhit = d['responses'][0]['hits']['total']
				_hits = d['responses'][0]['hits']['hits']
				self.__print('took: %d, nhits: %d' %(_took, _nhit))
				_msgs = []
				if _nhit > 0:
					res = 1
					for hit in _hits:
						#_msgs.append(hit['highlight']['message'][0])
						_msgs.append(hit['_source']['message'])
					return res, _took, _nhit, _hits, _msgs
				return [0,'no results found in the response :\n%s\n' %d]
		except Exception as e:
			print(traceback.format_exc())
			print (e)
			return False

	def __get_cookie(self, cj):
		'''
		extracts exabeam cookie from a cookie jar
		'''
		_CF = '/tmp/_ck.tmp'
		cj.save(_CF, ignore_discard=True)
		x = open(_CF, "r").readlines()
		os.remove(_CF)
		return('%s %s %s' %( x[1].split(" ")[1], x[2].split(" ")[1], 'ZGVzdD0iJSIyRmRhdGEiJSIyRmVsYXN0aWNzZWFyY2giJSIyRmV4YWJlYW0tMjAxOS4wNy4wOSIlIjJGX3NlYXJjaDs='.decode('base64')))

	def __to_epo(self, dt):
		e = datetime.datetime.utcfromtimestamp(0)
		return int((dt.replace(tzinfo=None) - e).total_seconds() * 1000.0)
	
	def __from_epo(self, dt):
		return datetime.datetime.fromtimestamp(dt / 1000.0).strftime('%c')

	def __print(self, msg):
		if Exabeam.VERBOSE:
			_caller = inspect.stack()[1][3]
			str = "%s - [%s] - %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), colo.format(_caller, 'CYAN'), msg)
			print (str)

	def __exa_request(self, exabeam_uri, mode='DL', data=None):
		
		_ref = Exabeam.EXABEAM_AA_URI + 'uba' if mode == 'AA' else Exabeam.EXABEAM_DL_URI + 'data/app/dataui'
		_cki = self.__get_cookie(self.aa_cj) if mode == 'AA' else self.__get_cookie(self.cj) 
		request = urllib2.Request(exabeam_uri)
		request.add_header('Content-Type', 'application/json')
		request.add_header('Accept','*/*')
		request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0')
		request.add_header('Referer', _ref)
		request.add_header('kbn-version', '5.1.1-SNAPSHOT')
		request.add_header('Cookie', _cki )
		try:
			res = urllib2.urlopen(request, context=Exabeam.ctx).read() if data is None else urllib2.urlopen(request, json.dumps(data), context=Exabeam.ctx).read()
		except urllib2.HTTPError, error:
			print 'Error #%s - %s' %(error.code, error.reason)
			self.__print(traceback.format_exc())
			return False
		except:
			print ('Communication Error')
			self.__print(traceback.format_exc())
			return False
		return res
		
	def test(self):
		d = datetime.datetime.now() - timedelta(days=30)
		print datetime.datetime.now(), self.__to_epo(datetime.datetime.now())
		print d, self.__to_epo(d)
	
	def exa_link_from_id(self, id, idx):
		return self.EXABEAM_DL_URI + '/data/app/dataui#/doc/exabeam-*/%s/logs?id=%s' %(idx, id)
	
	def getVersion(self):
		return __version__