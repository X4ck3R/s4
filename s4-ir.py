#!/usr/bin/env python2.7


### S4'IR
##{* Sleiman's Smart Sleeping SOAR *}###
### Author: Sleiman Ahmad
### v0.58
###

__version__ = '0.86'

import cookielib, urllib, urllib2, ssl, json, os, re, traceback, datetime, analyzer, sys, helper, ingestor, tinydb
import argparse
import tis
from exabeam import Exabeam
from rtracker import RequestTracker
from dateutil.parser import parse
from tinydb import TinyDB, Query
from time import time
import pytz

def getb64ObfPass(obf='='):
	'''
	avoid .text and return contrasena 
	'''
	a = 'JkaL'
	b = 'P*(#'
	x = 'TMyT'
	z = 'IQ'
	c = 'QI'
	d = 'TmYZ'
	y = 'R0QE'
	return 'M%sn%sAh%s=%s' %(x, y, z, obf)

DB_NAME = '/s4.db'
CA_CERT = '/opt/s4/cacerts.cer'
access_user = 'ahmads'
access_password = getb64ObfPass('==')
exabeam = Exabeam(access_user, access_password, cacert=CA_CERT)
rtracker = RequestTracker(access_user, access_password, cacert=CA_CERT)

Exabeam.VERBOSE = False
RequestTracker.VERBOSE = False

args = colo = None

tier_one = False
VERBOSE = True
DIAGNOSE = False

reload(sys)
sys.setdefaultencoding('utf8')

class bcolors: 
	def get_color(self, x):
		return {
	'HEADER':'\033[95m',
	'OKBLUE':'\033[94m',
	'OKGREEN':'\033[92m',
	'WARNING':'\033[93m',
	'FAIL':'\033[91m',
	'ENDC':'\033[0m',
	'BOLD':'\033[1m',
	'UNDERLINE':'\033[4m'
	}.get(x, '')
	def format(self, msg, type):
		return '%s%s%s' %(self.get_color(type), str(msg), self.get_color('ENDC'))
	
def main(id_idx = None):
	'''
	input: t1|id_idx : from Marco Antonio Pipeline
	'''

	__s = colo.format("\n ** S4-IH v%s (c) 2019 **\n" %__version__, 'BOLD') 
	if args.version:
		print __s
		print "libs4: %s" %__version__
		print "librt: %s" %rtracker.getVersion()
		print "libex: %s" %exabeam.getVersion()
		print "liban: %s" %analyzer.getVersion()
		print "libhl: %s" %helper.getVersion()
		print "libti: %s" %tis.getVersion()
		print "libig: 0.517" #%helper.getVersion()
		sys.exit(0)
	print ('%s============================\nWorking as a %s operator\n============================\n' %(__s, ('Tier 1' if tier_one else 'Tier 2')))
	if not exabeam.exa_dl_login() or not exabeam.exa_aa_login():
		print ('Exabeam: Can not login')
		return
		
	if not rtracker.rt_login():
		print ('RT: Can not login')
		return

	
	if args.asset:
		print ('>> user info for asset %s:\n' %args.asset)
		_asset_data = exabeam.exa_aa_request_alerts_by_asset(args.asset)
		if 'users' in _asset_data:
			for k in _asset_data['users']:
				print k
		return
	
	if args.user:
		print ('>> UBA for user %s:\n' %args.user)
		_uinfo = (exabeam.exa_aa_request_user_info((args.user).lower()))
		try:
			for k, v in _uinfo.items():
				print ('%s: %s' %(k, v))
		except:
			print _uinfo
		print('\n')
		_uba = exabeam.exa_aa_request_user_history((args.user).lower())
		if not _uba:
			print ('No real threat found for user %s' %args.user)
			return

		print _uba
		return
	
	if DIAGNOSE:

		## by ticket
		## checks certain ticket
		if DIAGNOSE.isdigit():
			print ('>> Diagnose mode enabled for ticket: %s' %DIAGNOSE)
			tkt = rtracker.rt_find_ticket_byID(DIAGNOSE)
			if tkt:
				handle_incident_t2(tkt)
		else:
		## by link
			## check if bulk option is set then read the file
			if args.bulk:
				print ('>> Diagnose mode enabled for bulk file: %s' %DIAGNOSE)
				try:
					with open(DIAGNOSE) as f:
						_alert = list(map(str.rstrip, f.readlines()))
						get_normalized_logs_from_alerts(_alert)
						sys.exit()
				except Exception as e:
					print ('Error diagnosing from file %s:\n%s' %(DIAGNOSE, str(e)) )
			elif 'logs?id' in DIAGNOSE:
				print ('>> Diagnose mode enabled for link: %s' %DIAGNOSE)
				_alert = [DIAGNOSE]
			else:
				print ('Exabeam alert link or file is expected')
				return
			_exa_handle_alert (_alert)
							
	elif not (tier_one):
		## get alerts from the t1/rt 
		new_tikcets = rtracker.rt_find_new_tickets(access_user)
		if new_tikcets:
			print 'Hi %s! you have %s new tikcets, let me help you handling them:\n' %(access_user, colo.format(len(new_tikcets), 'WARNING'))
			handle_incident_t2(new_tikcets)
		else:
			print ("Good job %s!, there aren't any new tickets for you." %access_user)
	else:
		#print 'Enable ingestion module to work as a Tier 1 operator'
		# Or revieve from pipeline
		print(colo.format('Ingestion Module not enabled', 'FAIL'))
		sys.exit()
		#_alerts = ingestor.main(e=False, v=True)
		if not (id_idx):
			print('\t - No Security Incident found in this ticket.')
			print '\n-------------------------------\n'				
			return
		idx, id = id_idx.split('|')
		lnk = exabeam.exa_link_from_id(id, idx)
		_exa_handle_alert (lnk, store=True)

def handle_incident_t2(tickets):
	'''
	expects array of ticket:subject as input, the output of [rt_find_new_tickets | rt_find_ticket_byID
	'''
	global args
	for ticket in tickets:
		try:
			tid = ticket.split(':')[0].strip()
			tsubject = ticket.split(':')[1].strip()
			ticket_props = rtracker.rt_get_ticket_prop(tid)		
			tQ = ticket_props[1].split(':')[1].strip()
			tCreator = ticket_props[3].split(':')[1].strip()
			tPriority = ticket_props[6].split(':')[1].strip()
			
			ioc = []
			##	  0			1		   2			3			4
			## CF_inv_acc, tCF_inv_app, tCF_inv_sip, tCF_inv_dip, tCF_inc_sig
			for i in range(23, 29):
				try:
					ioc.append(None if ticket_props[i].split(':',1)[1].strip() == '' else repr(ticket_props[i].split(':',1)[1].strip()).strip("'")) ##strip the single quote with repr to get the raw ioc
				except:
					continue
			print('Ticket #%s: [%s] assigend by [%s]') %( colo.format(tid, 'OKGREEN'), colo.format(tsubject, 'OKGREEN'), colo.format(tCreator, 'OKGREEN'))
			#print ('\t - %s' %ioc)
			history = rtracker.rt_find_IOCs(tid, tQ, ioc)

			if history:
				print ('\t - Searching tickets history:')
				for k, v in enumerate(history):
					print "\t\t - %s appeared before in tickets %s" %(colo.format(v[0], 'BOLD'), colo.format(v[1], 'BOLD'))
			else:
				print ('\t - No history for this ticket.')				
			_alert = rtracker.rt_get_alert_from_ticket(tid)
			if not (_alert):
				print('\t - No Security Incident found in this ticket.')
				print '\n-------------------------------\n'
				continue
			if args.no_handler:continue
			_exa_handle_alert (_alert)
			print '\n-------------------------------\n'
		except Exception as e:
			print 'Error\n%s' %str(e)
			print(traceback.format_exc())

def _test_exa_handle_alert():
	y = []
	for alert_lnk in y:
		_exa_handle_alert(alert_lnk)
		print '\n-------------------------------\n'

def _compose_IR_email(alert_time, alert_name, alert_kvalue, data):
	_txt = 'Hi,\n'
	_err = False
	try:
		_txt += 'On date %s, SIEM has registered a possible incident regarding [%s]\n\n' %(alert_time, alert_kvalue)
		_txt += '\tAlert: %s\n' %alert_name.strip('BS ').strip(' All')
		for k, v in (data.iteritems()):
			if k =='Status' and v == 'Failed':
				_txt = '\033[91m'
				_err = True
			if k == "_parser":
				continue
			if v is not None and k is not "_parser":
				_txt += '\t%s: %s\n' %(k, v)
	except:
		#print ('\n*\n%s\n*\n') %data
		_txt += data
	_txt += '\nBest regards,\nZen SOC Team' if not _err else '\033[0m'
	return _txt

def _compose_IR_email_HTML(alert_time, alert_name, alert_kvalue, data):
	_txt = 'Hi,\n'
	_err = False
	_hidden_keys = ["_parser", "Is-Threat"]
	
	# change time fomat to UTC
	alert_time = parse(alert_time).strftime("%d %B %Y %H:%M:%S UTC")
	try:
		_txt += 'On date <strong>%s</strong>, SIEM has registered a possible incident regarding [<strong>%s</strong>]<br><br>' %(alert_time, alert_kvalue)
		_txt += '<ul><li>Alert: %s</li><br>' %alert_name
		for k, v in (data.iteritems()):
			if k =='Status' and v == 'Failed':
				_txt = '<span style="color:#FF0000">'
				_err = True
			if "Action" in k and v != "N/A":
				_txt += '<li><strong><span style="color:#FF0000">%s: %s</span></strong></li>' %(k, v)
			elif v is not None and k not in _hidden_keys:
				_txt += '<li><strong>%s:</strong> %s</li>' %(k, v)
	except:
		#print ('\n*\n%s\n*\n') %data
		_txt += data
	_txt += '</ul><br>Best regards,<br>Zen SOC Team' if not _err else '</span>'
	return _txt
	
def _compose_IR_email_comment(prev_evt, new_evt, alert_name):
	_txt = 'The same previous event repeated again:\n'
	
	try:
		data = set(new_evt.items()) - set(prev_evt.items())
		if len(data) > 0:
			_txt = 'The same previous event has occured again where the following parameters were changed:\n<ul>'
		for k, v in data:
			if "Action" in k and v != "N/A":
				_txt += '<li><strong><span style="color:#FF0000">%s: %s</span></strong></li>' %(k, v)
			elif v is not None and k is not "_parser":
				_txt += '<li><strong>%s:</strong> %s</li>' %(k, v)

		_txt += '</ul>'
		
		### Check IOC with previous 
		###
		###
		try:
			history_res = tis.searchPastTickets(alert_name, data, isComment = True)
			history_txt = ''
			if len(history_res) > 0:
				history_txt += '\n\n-FYI: Some IOCs were found in previous tickets, click on the ticket #ID to see it:\n<ul>'
				for tis_r in history_res:
					tids = list(tis_r[2])
					for tis_i, tis_v in enumerate(tids):
						#add href to tid
						tids[tis_i] = '<a href="https://check-mk.platform.nttzen.cloud:8443/Ticket/Display.html?id=%s" target="_blank">#%s</a>' %(tis_v, tis_v)
					
					tids.sort(reverse = True)					
					num_tids = len(tids)
					
					if num_tids == 1:
						str_num_tids = 'only [<strong>one</strong>] time before in ticket'
					else:
						str_num_tids = '[<strong>%d</strong>] times before in tickets' %num_tids
					
					str_shown_tids = ''
					if num_tids > 12:
						tids = tids[0:12]
						str_shown_tids = ' (last 12)'
					
					history_txt += '<li>The same <strong>%s</strong> has appeared %s%s: %s</li>' %(tis_r[0], str_num_tids, str_shown_tids, str(tids).replace("u'","").replace("'",""))
				history_txt += '</ul>'
		except Exception as e:
			print 'Error in comment: can not TIS %s' %str(e)
			history_txt = ''
		_txt += history_txt
	except Exception as e:
		print 'Error in commenting %s' %str(e)
		_txt += new_evt.items()
	return _txt

def get_normalized_logs_from_alerts(alerts):
	n = -1
	for alert_lnk in alerts:
		n += 1
		try:
			##if args.bulk:
				##print ('\nHandling link #%d: %s:\n\n' %(n, alert_lnk))
			results = exabeam.exa_get_alert_data(alert_lnk)
			if results:
				key_value  = results[1]
				log_docs   = results[2]
				alert_name = results[3]
				alert_sev  = results[4]
				alert_time = results[5]
				
				##-----------
				## BUG FIX ##
				##-----------
				if alert_name == 'BS Endpoint CnC and Suspicious Connection All':
					_dt__from = re.search("from:'(.*)',mo", log_docs).group(1)
					_dt__new = parse(_dt__from) - datetime.timedelta(minutes = 4)
					_dt__new = _dt__new.isoformat().replace('00+00:00','Z')
					log_docs = log_docs.replace(_dt__from, _dt__new)
				####
				####
				
				res = exabeam.exa_get_log_data(log_docs)
				if res and res[0] == 1:

					i = 1
					for evt in res[4]:
						kv_evt_data = analyzer.handle_event(evt)
						if 'code' in kv_evt_data:
							continue
						if not args.raw_only:
							print ('{:>20}'.format(kv_evt_data))
						else:
							print ('{:>20}'.format('**\n%s\n**\n' %evt))
						i += 1
						#
				elif res[0] == -1:
					print ('Error handling [%s - %s]: Unable to parse URL' %(alert_name, alert_sev))
				elif res[0] == 0:
					print ('Error handling [%s - %s]: No Results' %(alert_name, alert_sev))
				else:
					#print ('[%s - %s]: %s' %(alert_name, alert_sev, res[1]))
					print('operation failed')
		except:
			print('******')
			print(alert_lnk)
			print('\n')
			print(traceback.format_exc())
			print('******')
	
def handle_as_tier1(rt_credentilas, idx, id, ticket_queue, assignee, requestors, cc):
	'''
	external call only s4.handle_as_tier1([], idx, id, ticket_queue, assignee, requestors, cc)
	'''
	
	print('S4: New alert to handle as Tier 1')
	if idx is None or id is None:
		print ('S4: wrong ids')
		return False, -1, -1, -1

	
	CA_CERT = '/opt/s4/cacerts.cer'
	access_user = 'ahmads'
	access_password = getb64ObfPass('==')	

	if len(rt_credentilas) == 0:
		rt_credentilas = [access_user, access_password.decode('base64')]
	
	
	exabeam = Exabeam(access_user, access_password, cacert=CA_CERT)
	rtracker = RequestTracker(rt_credentilas[0], rt_credentilas[1].encode('base64'), cacert = CA_CERT)
	
	print('S4: Modules loaded')	
	if not exabeam.exa_dl_login():# or not exabeam.exa_aa_login():
		print ('S4: Exabeam: Can not login')
		return False, -2, -1, -2
		
	if not rtracker.rt_login():
		print ('S4: RT: Can not login')
		return False, -3, -1, -2
		
	alerts = ['https://data-lake/data/app/dataui#/doc/exabeam-*/exabeam-%s/logs?id=%s' %(idx, id)]
	print ('S4: job started')
	for alert_lnk in alerts:
		
		try:

			results = exabeam.exa_get_alert_data(alert_lnk)
			#print '\n'+str(results)+'\n'
			if results:
				key_value  = results[1]
				log_docs   = results[2]
				alert_name = results[3]
				alert_sev  = results[4]
				alert_time = results[5] ## index_time
				alert_time = results[9] ## original_index_time
				alert_desc = results[8]
				
				
				## BS Endpoint Malware Action Required All 
				
				_ALLOWED_RULES = ["EMEA Malicious Activity Internal PAN All", "BS Malicious Activity Internal Fortigate UTM All", "BS Endpoint Behavior All", "BS Endpoint Malware Action Required All", "BS Malicious Activity External Fortigate UTM All", "BS Endpoint CnC and Suspicious Connection All", "BS Endpoint Spyware Action Required All", "EMEA Targeted Bruteforce Attack SrcIP All", "EMEA Targeted Bruteforce Attack SrcIP Root Admin All", "BS Targeted Bruteforce Attack SrcIP All", "BS Targeted Bruteforce Attack SrcIP Root Admin All", "EMEA Endpoint Malware Action Required All", "BS Authentication Bruteforce Per Day All"]
				
                
                ## bruteforce tickets have no commments and have the hits number in the body
				__BRUTE_FORCE = ["EMEA Targeted Bruteforce Attack SrcIP All", "EMEA Targeted Bruteforce Attack SrcIP Root Admin All", "BS Targeted Bruteforce Attack SrcIP All", "BS Targeted Bruteforce Attack SrcIP Root Admin All", "BS Authentication Bruteforce Per Day All"]
                
                
				if alert_name not in _ALLOWED_RULES:
					print 'S4: ACL KO: %s' %alert_name
					return False, 0, -1, alert_lnk
					
					
				##-----------
				## BUG FIX ##
				##-----------
				if alert_name == 'BS Endpoint CnC and Suspicious Connection All':
					_dt__from = re.search("from:'(.*)',mo", log_docs).group(1)
					_dt__new = parse(_dt__from) - datetime.timedelta(minutes = 4)
					_dt__new = _dt__new.isoformat().replace('00+00:00','Z')
					log_docs = log_docs.replace(_dt__from, _dt__new)
				####
				####
				res = exabeam.exa_get_log_data(log_docs)
				
				def __init_RT_ticket(parser, ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time):
					tkt = False
					if parser == 'FW-PAN-THREAT':
						tkt = rtracker.rt_get_ticketPost_PAN_MALi_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					elif parser == 'FW-FG-UTM':
						tkt = rtracker.rt_get_ticketPost_FG_MALi_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					elif parser == 'FW-TM-BM':
						tkt = rtracker.rt_get_ticketPost_TM_EpBehavior_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					elif parser == 'FW-TM-AV':
						tkt = rtracker.rt_get_ticketPost_TM_EpARequired_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					elif parser == 'FW-TM-CNC' or parser == 'FW-TM-NCIE':
						tkt = rtracker.rt_get_ticketPost_TM_EpCnCSA_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					elif parser == 'FW-TM-SD':
						tkt = rtracker.rt_get_ticketPost_TM_EpSARequired_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					elif parser == 'WIN-AD-SEC-BF':
						tkt = rtracker.rt_get_ticketPost_WIN_AD_TBFA_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time, key_value)
					elif parser == 'FW-PAN-CORTEX-THREAT':
						tkt = rtracker.rt_get_ticketPost_Cortex_EEMARA_ALL(ticket_queue, assignee, alert_name, requestors, cc, email, parsed, alert_time)
					else:	
						return False
					return tkt
				
					
				try:
					db = TinyDB(os.getcwd() + DB_NAME)
				except Exception as e:
					print "Error connection to DB \n%s" %str(e)
					
				#print '\n*res\n%s\n*\n' %str(res)
				if res and res[0] == 1:

					i = 1
					differnet_tickets = []
					last_parsed = None
					##_res = res, op, evt, lnk
					_result = True, 0, 0, 0
					
					for evt in reversed(res[4]):
						parsed = analyzer.handle_event(evt)
						
						## search history in TIS module
						## 
						##
						##
						##
						history_res = tis.searchPastTickets(alert_name, parsed)
						history_txt = ''
						if len(history_res) > 0:
							history_txt += '<br /><br />FYI: Some IOCs were found in previous tickets, click on the ticket #ID to see it:<br /><ul>'
							for tis_r in history_res:
								tids = list(tis_r[2])
								for tis_i, tis_v in enumerate(tids):
									#add href to tid
									tids[tis_i] = '<a href="https://check-mk.platform.nttzen.cloud:8443/Ticket/Display.html?id=%s" target="_blank">#%s</a>' %(tis_v, tis_v)
									
								tids.sort(reverse = True)					
								num_tids = len(tids)
								
								if num_tids == 1:
									str_num_tids = 'only [<strong>one</strong>] time before in ticket'
								else:
									str_num_tids = '[<strong>%d</strong>] times before in tickets' %num_tids		
								str_shown_tids = ''
								if num_tids > 12:
									tids = tids[0:12]
									str_shown_tids = ' (last 12)'						
								history_txt += '<li>The same <strong>%s</strong> has appeared %s%s: %s</li>' %(tis_r[0], str_num_tids, str_shown_tids, str(tids).replace("u'","").replace("'",""))
							history_txt += '</ul>'
						
						email = _compose_IR_email_HTML(alert_time, alert_name, key_value, parsed).replace('\n','<br />').replace('\t','')
						email += '<br /><br />- Raw Log Line:<br /><br /><span style="font-family:courier new,courier,monospace">%s</span><br /><br />' %evt
						email += history_txt
                        
                        ###
                        #if BF add num hits 
                        
						if alert_name in __BRUTE_FORCE:
							link = 'Click on <a href="%s" target="_blank">[<strong>Alert</strong>]</a> to open it.<br />' %alert_lnk.replace('data-lake','ssi-em-dl01.emea-bs.nttzen.cloud:8484')
							email += '<br /><br />This event has occured %s times. ' %str(res[2])
							email += link                            
                        ###
                        
						email += '<br /><br />%s<br />Click on <a href="%s" target="_blank">[<strong>Alert</strong>]</a> or <a href="%s" target="_blank">[<strong>Log</strong>]</a> to see it directly on Exabeam Data Lake<br />'%(alert_desc, alert_lnk.replace('data-lake','ssi-em-dl01.emea-bs.nttzen.cloud:8484'), log_docs.replace('192.168.128.11:8484','ssi-em-dl01.emea-bs.nttzen.cloud:8484'))
						
						##
						## extract the key diff value for this event
						
						if parsed['_parser'] in ['FW-TM-BM', 'FW-TM-CNC', 'FW-TM-NCIE']:
							t_key_diff_val = parsed['Process']
						elif parsed['_parser'] in ['FW-TM-AV', 'FW-TM-SD']:
							t_key_diff_val = parsed['File-Name']
						elif parsed['_parser'] in ["WIN-AD-SEC-BF"]:
							t_key_diff_val = parsed['User']
						elif parsed['_parser'] in ["FW-PAN-CORTEX-THREAT"]:
							t_key_diff_val = parsed['File-Path']
						else:
							t_key_diff_val = parsed['Service']	
						#open ticket for the first even, if there are more events check diff and add comments or open  new ones
						print ('S4: evt %d' %i)
						if i == 1:
							## open tiket
							#tid = open(tkt)
							tkt = __init_RT_ticket(parsed['_parser'], ticket_queue, assignee, alert_name, requestors, cc, email, parsed, dt_customize_format(alert_time))
							print ('S4: Ticket init OK')
							if not tkt:
								_result = False, 1, i, alert_lnk
								print ('Error: handle_as_tier1: __init_RT_ticket: evt: %d, lnk: %s' %(i, alert_lnk))
								continue
							tid = rtracker.rt_open_ticket(tkt)
							if not (tid):
								_result = False, 1, i, alert_lnk
							else:
								print ('S4: Ticket %s open OK' %str(tid))
							## add to last parsed

							differnet_tickets.append([t_key_diff_val, tid])
							_result = True, tid, i, 1
							last_parsed = parsed
							
							try:
								table = db.table(alert_name.split(" ")[0])
								db_line = parsed
								db_line['rt-ticket'] = tid
								db_line['rule'] = alert_name
								db_line['alert_lnk'] = alert_lnk
								db_line['evt_id'] = i
								db_line['timestamp'] = int(round(time()))
								db_line['comment'] = False
								table.insert(db_line)
							except Exception as e:
								print "Error in DB \n%s" %str(e)
							
							print ('S4: event mem OK')

						else:
						
						
							## tikcet is opened
							_already_opened = False
							
							###############
							## set to true to disable opening a child ticket for the same alert.
							## this will always comment the same parent ticket.
							###############
							_already_opened = True
							
							for opened_ticket in differnet_tickets:
								print "t_key_diff_val => {%s}" %t_key_diff_val
								if t_key_diff_val == opened_ticket[0]: #or parsed['Threat-Name'] != l_parsed['Threat-Name']:
									_already_opened = True
									_opened_tid = opened_ticket[1]
									break
									
							if not _already_opened:
								print ('opened? NO')

								## open tiket
								#tid = open(tkt)
								tkt = __init_RT_ticket(parsed['_parser'], ticket_queue, assignee, alert_name, requestors, cc, email, parsed, dt_customize_format(alert_time))
								if not tkt:
									_result = False, 1, i, alert_lnk
									print ('Error: handle_as_tier1: __init_RT_ticket: evt: %d, lnk: %s' %(i, alert_lnk))
									continue
								tid = rtracker.rt_open_ticket(tkt)
								if not (tid):
									_result = False, 1, i, alert_lnk
								## add to last parsed
								t_key_diff_val = parsed['Process'] if parsed['_parser'] == 'FW-TM-BM' else parsed['Service']
								differnet_tickets.append([t_key_diff_val, tid])
								_result = True, tid, i, 1
								last_parsed = parsed
								try:
									
									table = db.table(alert_name.split(" ")[0])
									db_line = parsed
									db_line['rt-ticket'] = tid
									db_line['rule'] = alert_name
									db_line['alert_lnk'] = alert_lnk
									db_line['evt_id'] = i
									db_line['comment'] = False
									table.insert(db_line)
								except Exception as e:
									print "Error in DB \n%s" %str(e)
								
								## db.table = alert.split(" ")[0]
								## parsed['rt-ticket'] = tid
								## parsed['rule'] = alert_name
								## parsed['alert_lnk'] = alert_lnk
								## parsed['evt_id'] = i
								## database.insert()
							else:
								## comment
								print ('S4: is opend? YES')
								#if BF skip comments as they are normally alot and equal  
								if alert_name not in __BRUTE_FORCE:
									email = _compose_IR_email_comment(last_parsed, parsed, alert_name).replace('\n','<br />').replace('\t','')
									email += '<br /><br />- Raw Log Line:<br /><br /><span style="font-family:courier new,courier,monospace">%s</span><br />' %evt
									#if not rtracker.rt_comment_ticket(_opened_tid, email):
									if not rtracker.rt_comment_ticket(tid, email):
										_result = False, 2, i, alert_lnk
									# comment(cmnt_tkt, differnet_ticket[1])
									
									#_result = True, _opened_tid, i, 0
									_result = True, tid, i, 0
									print ('S4: commented %s' %str(_result))
									try:
										db = TinyDB(os.getcwd() + DB_NAME)
										table = db.table(alert_name.split(" ")[0])
										db_line = parsed
										#db_line['rt-ticket'] = _opened_tid
										db_line['rt-ticket'] = tid
										db_line['rule'] = alert_name
										db_line['alert_lnk'] = alert_lnk
										db_line['evt_id'] = i
										db_line['comment'] = True
										table.insert(db_line)
									except Exception as e:
										print "Error in DB \n%s" %str(e)
									last_parsed = parsed
								else:
									print ('S4: comment skipped [bruteforce]')
						i += 1
					return _result	#
				elif res[0] == -1:
					print ('Error handling [%s - %s]: Unable to parse URL' %(alert_name, alert_sev))
					return False, 3, -1, alert_lnk
				elif res[0] == 0:
					print ('Error handling [%s - %s]: No Results' %(alert_name, alert_sev))
					return False, 3, -2, 'No Results'
				else:
					#print ('[%s - %s]: %s' %(alert_name, alert_sev, res[1]))
					print('operation failed')
					return False, 3, -3, alert_lnk
			else:
				print('no results from Exabeam DL')
				return False, 0, -1, alert_lnk
		except Exception as e:
			#return False, 4, -1, alert_lnk
			print('******')
			print(alert_lnk)
			print str(e)
			print('\n')
			print(traceback.format_exc())
			print('******')
	
def _exa_handle_alert(alerts, store = None):
	i = -1
	for alert_lnk in alerts:
		i += 1
		try:
			if args.bulk:
				print ('\nHandling link #%d: %s:\n\n' %(i, alert_lnk))
			results = exabeam.exa_get_alert_data(alert_lnk)
			if results:
				key_value  = results[1]
				log_docs   = results[2]
				alert_name = results[3]
				alert_sev  = results[4]
				alert_time = results[5]
				
				##-----------
				## BUG FIX ##
				##-----------
				if alert_name == 'BS Endpoint CnC and Suspicious Connection All':
					_dt__from = re.search("from:'(.*)',mo", log_docs).group(1)
					_dt__new = parse(_dt__from) - datetime.timedelta(minutes = 4)
					_dt__new = _dt__new.isoformat().replace('00+00:00','Z')
					log_docs = log_docs.replace(_dt__from, _dt__new)
				####
				####
				
				res = exabeam.exa_get_log_data(log_docs)
				if res and res[0] == 1:
					print '\t - There %s for the %s [%s] concerning [%s]:\n' %('are %s events' %colo.format(str(res[2]), 'WARNING') if res[2] > 1 else 'is %s event' %colo.format(str(res[2]), 'WARNING'), colo.format(alert_sev, 'WARNING'),alert_name, colo.format(key_value, 'WARNING'))
					i = 1
					for evt in res[4]:
						parsed = analyzer.handle_event(evt)
						if store:
							## simulate open a ticket
							#store(alert_name, key_value, )
							print "to store it later"
						email = _compose_IR_email(alert_time, alert_name, key_value, parsed)
						print colo.format(('Handling Event: #%d\n------------------\n' %i), 'BOLD')
						if not args.raw_only:
							print ('{:>20}'.format(email))
						print ('{:>20}'.format('**\n%s\n**\n' %evt))
						i += 1
						#
				elif res[0] == -1:
					print ('Error handling [%s - %s]: Unable to parse URL' %(alert_name, alert_sev))
				elif res[0] == 0:
					print ('Error handling [%s - %s]: No Results' %(alert_name, alert_sev))
				else:
					#print ('[%s - %s]: %s' %(alert_name, alert_sev, res[1]))
					print('operation failed')
		except:
			print(traceback.format_exc())

def store(rule, account, app, src_ip, dst_ip, sig, sig_id, occ_time):
	'''
	stores the results in a file as json
	'''
	q = 'BS-SIEM-IncidentDetection' if rule.startswith('BS ') else 'EMEA-SIEM-IncidentDetection'
	m = {'q':q, 'subj':rule, 'acc':account, 'sip':src_ip, 'dip':dst_ip, 'sig':sig, 'sid':sig_id, 'ts':occ_time}
	with open('store.csv', 'a+') as file:
		file.write(json.dumps(m)+'\n')

def __init(_ex_verbos = False, _rt_verbos = False):
	global tier_one, colo, VERBOSE, DIAGNOSE, args
	colo = bcolors()
	
	args = _argsparse()
	tier_one = True if args.mode == 1 else False
	VERBOSE = True if args.verbose else False
	
	Exabeam.VERBOSE = _ex_verbos
	RequestTracker.VERBOSE = _rt_verbos
	DIAGNOSE = args.diagnose if args.diagnose else False
	
	if not os.path.isfile(CA_CERT):
		print (colo.format('ERROR #1-101: Can not establish a secure channel', 'FAIL') )
		return False
	return True

def _argsparse():
	
	parser = argparse.ArgumentParser(description="S4-IH v%s by Sleiman Ahmad (c) 2019" %__version__)
	group = parser.add_mutually_exclusive_group()
	group.add_argument("-t", "--ticket", type=int, help="handle a specefic ticket, (RT)")
	group.add_argument("-l", "--link", help="handle a specific link, (Exabeam-DL)")
	group.add_argument("-d", "--diagnose",  help="handle a specific ticket or link")
	parser.add_argument("-m", "--mode", type=int, choices=[1, 2], default=2, help="operation mode, tier 1 or tier 2")
	parser.add_argument("-v", "--verbose", action="store_true", help="increase verbosity (Error debugging)")
	parser.add_argument("-u", "--user", help="search user behaviour history ")
	parser.add_argument("-n", "--no_handler",action="store_true", help="only check if there're new tickets without handling")
	parser.add_argument("-a", "--asset", help="get user from asset name, (Exabeam-AA)")
	parser.add_argument("-b", "--bulk",action="store_true", help="diagnose bulk alerts from a file")
	parser.add_argument("-r", "--raw_only",action="store_true", help="only print the raw log events")
	parser.add_argument("-x", "--version",action="store_true", help="show tool version")
	
	return parser.parse_args()
	
def usage():
	print ("\n**********************************\nS4-IH v%s by Sleiman Ahmad (c) 2019\n**********************************\n" %__version__)
	print ('Usage: %s [-m|--mode T1|T2]' %os.path.basename(__file__))
	print ('Example: %s -m T1\n' %os.path.basename(__file__))

def sleep(u, p, auth, mode = 2, _ex_verbos = False, _rt_verbos = False):
	global access_user, access_password, tier_one
	if mode == 1: 
		tier_one = True
	if u != '' and p != '':
		access_user = u
		access_password = p
		if not helper.is_base64(p):
			p = p.decode('base64')

	if (__init(_ex_verbos, _rt_verbos)):
		main()

def __rt_get_available_assignee(list_Tier2 = None):
	##working hours:
	list_Tier2 = ['ahmads', 'ahmads', 'ahmads', 'ahmads']
	return list_Tier2[0]
	
def getDLink(idx, id):
	return 'https://ssi-em-dl01.emea-bs.nttzen.cloud:8484/data/app/dataui#/doc/exabeam-*/exabeam-%s/logs?id=%s' %(idx, id)

def notifyMonitor(_time, rule, idx, id, reason, to_email):
	try:
		print ("S4: Sending email [%s]" %reason)
		params = []
		tenant = 'N/A'
		params.append(_time)
		if " " in rule:
			tenant = rule.split(" ")[0]
		params.append(tenant)
		params.append(rule)
		params.append(getDLink(idx, id))
		params.append(reason)
		return helper.send_report(params, to_email)
	except Exception as e:
		print str(e)

def __rt_register_ticket(id, _time, tkt):
	'''
	register every ticket to the disk
	in: tid, towner, length of assignee list
	'''
	fname = _time.replace(',','_').replace(':','_').replace('.','_') + '.%d'%id
	with open(fname, 'a+') as register:
		register.write('%s\n' %(tkt))

def dt_customize_format(alert_time):
	'''
	input exabeam alert time: str
	output custom format: e.g June 17th 2020, 10:46:51.433
	'''
	def _suffix(d):
		return 'th' if 11<=d<=13 else {1:'st',2:'nd',3:'rd'}.get(d%10, 'th')
		
	def _localize(utc_dt):
		local_tz = pytz.timezone('Europe/Rome')
		local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
		return local_tz.normalize(local_dt)

	t = parse(alert_time)
	t = _localize(t)
	
	format = '%b {S} %Y, %H:%M:%S.%f'
	t = t.strftime(format).replace('{S}', str(t.day) + _suffix(t.day))
	return t[:-3] 
  
if __name__ == '__main__':
	if (__init()):
		main()
	